+++
title = "Weekly GNU-like Mobile Linux Update (26/2022): Plasma Mobile Gear 22.06, Ubuntu Touch OTA-23 and the MNT Pocket Reform"
date = "2022-07-03T21:45:00Z"
draft = false
[taxonomies]
tags = ["PinePhone Pro","USB-C","Megapixels","PINE64 Community Update","Ubuntu Touch","Sailfish OS","Phosh","MNT Pocket Reform","Plasma Mobile",]
categories = ["weekly update"]
authors = ["peter"]
+++
Extensions coming to GNOME Web, some new Sailfish OS Community News, NOKIA causes a naming dispute and more!
<!-- more -->
_Commentary in italics._

### Hardware announcements
* MNT:  [Introducing MNT Pocket Reform](https://mntre.com/media/reform_md/2022-06-20-introducing-mnt-pocket-reform.html). _A nice piece of hardware, incredibly modular. Sadly, modularity affects size negatively, at 20 × 12.6 × 4.5cm this device is not for every pocket. Still liking it a lot!_
  * TuxPhones: [The PocketReform is a made-in-Berlin Linux handheld](https://tuxphones.com/mnt-pocketreform-open-hardware-linux-pda-keyboard-arm/).

### Software progress

#### GNOME ecosystem
* This Week in GNOME: [#50 Extend the Web](https://thisweek.gnome.org/posts/2022/07/twig-50/). _AdwMessageDialog is one of these small features with (likely) big impact._
* TingPing's blog: [WebExtension Support in Epiphany](https://blog.tingping.se/2022/06/29/WebExtensions-Epiphany.html). _Sadly, gnome-nightly does not seem to include this for AARCH64, making easy testing on our 64-bit ARM mobile devices impossible. Still: Great to see this happening, really looking forward to this landing soon!_
* Marcus Lundblad: [Summer Maps](http://ml4711.blogspot.com/2022/06/summer-maps.html).

##### Releases
* [Phoc 0.21.0_beta1](https://gitlab.gnome.org/World/Phosh/phoc/-/tags/v0.21.0_beta1), [Phosh 0.20.0_beta2](https://gitlab.gnome.org/World/Phosh/phosh/-/releases/v0.20.0_beta2) and [Calls 43 alpha 2](https://gitlab.gnome.org/GNOME/calls/-/releases/v43_alpha.2) were released in the past week.

#### Plasma/Maui ecosystem
* Plasma Mobile: [Plasma Mobile Gear ⚙ 22.06 is Out](https://plasma-mobile.org/2022/06/28/plasma-mobile-gear-22-06/). _Great improvements!_
* Nate Graham: [This week in KDE: And now time for some UI polishing](https://pointieststick.com/2022/07/01/this-week-in-kde-and-now-time-for-for-some-ui-polishing/). _Nice progress!_
* Felipe Kinoshita: [My week in KDE: Some Nice Stuff](https://fhek.gitlab.io/en/my-week-in-kde-some-nice-stuff/).

#### Ubuntu Touch
* Florian Leeber for UBports: [Ubuntu Touch OTA-23 Release](http://ubports.com/blog/ubports-news-1/post/ubuntu-touch-ota-23-release-3855). _Nice improvements around Wireless Displays, FM Radio and MMS handling!_
* Jeroen Baten for UBports: [Hi human! Your Biweekly UBports news is here!](http://ubports.com/blog/ubports-news-1/post/hi-human-your-biweekly-ubports-news-is-here-3856).

#### Sailfish OS
* flypig: [Sailfish Community News, 30th June, Microtube](https://forum.sailfishos.org/t/sailfish-community-news-30th-june-microtube/12130).

#### Distributions
* Mobian: [This month in Mobian: June 2022](https://blog.mobian.org/posts/2022/06/30/tmim/). _Lots of great progress in Mobian's realm, personally (of course) the patches (already merged upstream) to GNOME Text Editor excite me the most! Also: Kudos for discussing mishaps like that key expiry openly!_
* Kupfer Linux: [v0.1.1: Kupferbootstrap Documentation](https://kupfer.gitlab.io/blog/2022-06-28_v0-1-1_and_docs.html) _Nice progress!_

#### Apps
* Megapixels saw [two minor bugfix releases this week](https://gitlab.com/postmarketOS/megapixels/-/tags), bumping the release to 1.5.2[.](https://fosstodon.org/web/@linmob/108585098622194354)
* [Flare, a WIP Signal client](https://gitlab.com/Schmiddiii/flare) has been released on [Flathub](https://flathub.org/apps/details/de.schmidhuberj.Flare) this week. _Still early days and a bit crashy in my testing, but definitely worth a look!_

#### Graphics
* Phoronix: [Etnaviv Gallium3D Lands Async Shader Compilation With ARB_parallel_shader_compile](https://www.phoronix.com/scan.php?page=news_item&px=Etnaviv-Async-Shader-Compile).
* Phoronix: [Mesa's Lima Gallium3D Driver Lands 4x MSAA Support](https://www.phoronix.com/scan.php?page=news_item&px=Lima-4x-MSAA-Mesa-22.2).


### Worth noting
* If you're interested in daily driving your PinePhone, this [reddit thread](https://teddit.net/r/pinephone/comments/vn9gpw/just_recently_got_my_pinephone_whats_currently/) might help.
* Customer experience matters. If someone has bought more than one of your products, please don't treat them [like this](https://forums.puri.sm/t/librem-5-buyer-beware/17706)! _I usually don't include random complaints, but this one is just sad._
* [Lawyers gonna lawyer](https://twitter.com/ReimuNotMoe/status/1542466662154108930#m): What we know and love as "Notkia" [is looking for a new name](https://twitter.com/ReimuNotMoe/status/1542422377148977152#m)!

### Worth reading

#### PINE64 Community Update
* PINE64: [June Update: Who likes RISC-V?](https://www.pine64.org/2022/06/28/june-update-who-likes-risc-v/). _PinePhone (Pro) availability, PineNote Progress are the best

#### PinePhone Pro USB-C
* megi's PinePhone Development Log: [Pinephone Pro Type-C support is now complete](https://xnux.eu/log/#071). _Great work! I hope this can somehow trickle down to other RK3399 based devices (looks at \*RockPro64\*)._

#### PinePhone Pro Camera Success
* twongkee's Blog: [PinephonePRO camera working in July 2022](https://wongkee.ca/2022/07/02/pinephonepro-camera-working-in-july-2022/). _Not too bad, more details can be found on [reddit](https://www.teddit.net/r/pinephone/comments/vpx1vr/pinephonepro_camera_working_in_july_2022/)._

#### CEO Interviews
* The Register: [The App Gap and supply chains: Purism CEO on what's ahead for the Librem 5 USA](https://www.theregister.com/2022/07/02/interview_purism_ceo_librem_usa/). _Money to fill the app gap? Sounds good. I hope that when Purism add this as a feature to PureOS Store, they do it in a way that makes use on non-Purism devices possible, including payments. I vaguely recall some old ideas to base this all on flatpak, which would make this possible. Also interesting: Details on a possible next-gen Librem phone._
  * [Comment thread](https://forums.puri.sm/t/the-register-interviews-todd-weaver/17681/17) on Purism's forums.

#### Fluff
* Purism: [How to Challenge Big Tech with Privacy-First Alternatives](https://puri.sm/posts/how-to-challenge-big-tech-with-privacy-first-alternatives/).

### Worth listening
* postmarketOS Podcast: [#19 apk fix, qbootctl, Phosh 0.20, iPhones and more Apples](https://cast.postmarketos.org/episode/19-apk-fix-qbootctl-phosh-0.20-iPhones-and-more-Apples/).[^1] _Another great episode!_
* Software Freedom Podcast: [SFP#15: All about Upcycling Android](https://fsfe.org/news/podcast/episode-15.en.html). _A bit older, but I only found it now. :-)_
* [GERMAN] GnuLinuxNews Podcast: [GLN024 - Bewusste Maschinen, Massenüberwachung, Replicant, Gewinner](https://gnulinux.ch/gln024-podcast). _If you can understand German, the segment on Massenüberwachung has some good thoughts about the FOSS mobile space, starting around minute 54._

### Worth watching

#### Plasma Mobile
* Avisando: [Manjaro Plasma Mobile New Features (PinePhone)](https://www.youtube.com/watch?v=7_evVme7E7k). _Nice progress!_

#### AIX 4.3 on PinePhone
* Doctor Cranium: [AIX 4.3 on a PinePhone](https://www.youtube.com/watch?v=jGEP5DQLFhQ). _[AIX](https://en.wikipedia.org/wiki/IBM_AIX) is an old UNIX._

#### PINE64 Community Update
* PINE64: [June Update: Who likes RISC-V?](https://www.youtube.com/watch?v=lqERznpxsPE). _Another great synopsis by PizzaLovingNerd!_

#### Development streams
* caleb: [postmarketOS mainline SDM845 SHIFT6mq displayport alt mode hacking](https://www.youtube.com/watch?v=hQONrDrgXPk).

### Something missing?
If your project's cool story (or your awesome video or nifty blog post or ...) is missing and you don't want that to happen again, please get in touch via social media or email!

PS: In case you are wondering about the title [...](https://fosstodon.org/@linmob/108516506484897358)

[^1]: This excellent episode was initially missed and added early on June 4th.
