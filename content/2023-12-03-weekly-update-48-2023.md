+++
title = "Weekly GNU-like Mobile Linux Update (48/2023): Jollaboys Ltd. and MauiKit 4"
date = "2023-12-03T22:05:42Z"
draft = false
[taxonomies]
tags = ["Maui Project", "Sailfish OS", "Ubuntu Touch", "Nemo Mobile", "postmarketOS", "PineTab 2", "Phosh",]
categories = ["weekly update"]
authors = ["Peter",]
[extra]
author_extra = " (with friendly assistance from plata's awesome script)"
+++

Jolla no longer in Russian hands, another NemoMobile update, MauiKit 4 release briefing, a UBports Q&A, how to become a postmarketOS Trusted Contributor (and the week of v23.12 testing begins), Phoah Fashion, user0's Firefox customizations now on codeberg and check out Worth Reading. 
<!-- more -->
_Commentary in italics._

### Software

#### Gnome Ecosystem
- This Week in GNOME: [#124 Fixes and Improvements](https://thisweek.gnome.org/posts/2023/12/twig-124/)
- bilelmoussaoui: [Reducing Mutter dependencies](https://belmoussaoui.com/blog/18-reducing-mutter-dependencies/)

#### Phosh
- Phosh.mobi blog: [Phosh Fashion](https://phosh.mobi/posts/phosh-fashion/)

#### Plasma Ecosystem
- Nate Graham: [This week in KDE: changing the wallpaper from within System Settings](https://pointieststick.com/2023/12/01/this-week-in-kde-changing-the-wallpaper-from-within-system-settings/)
- Volker Krause: [October/November in KDE Itinerary](https://www.volkerkrause.eu/2023/12/02/kde-itinerary-october-november-2023.html)
- KDE Annoucements: [KDE's 6th Megarelease - Beta 1](https://kde.org/announcements/megarelease/6/beta1/)
- redstrate: [My work in KDE for November 2023](https://redstrate.com/blog/2023/11/my-work-in-kde-for-november-2023/)
- Phoronix: [KDE Plasma 6.0 Beta 1 Released With Frameworks & Gear Updated](https://www.phoronix.com/news/KDE-Plasma-6.0-Beta)

#### Maui Project
- MauiKit blog: [Maui Release Briefing # 4](https://mauikit.org/blog/maui-release-briefing-4/) _Impressive!_

#### Sailfish OS
- Community News - Sailfish OS Forum: [Sailfish Community News, 30th November - Jolla reborn!](https://forum.sailfishos.org/t/sailfish-community-news-30th-november-jolla-reborn/17473)
- [Former\_leadership\_buys\_Jolla\_Business\_Pressrelease\_271123\_.pdf](https://jolla.com/content/uploads/2023/11/Former_leadership_buys_Jolla_Business_Pressrelease_271123_.pdf?x25613)
- flypig's Gecko log: [Day 95](https://www.flypig.co.uk/list?list_id=941&list=gecko)
- flypig's Gecko log: [Day 94](https://www.flypig.co.uk/list?list_id=940&list=gecko)
- flypig's Gecko log: [Day 93](https://www.flypig.co.uk/list?list_id=939&list=gecko)
- flypig's Gecko log: [Day 92](https://www.flypig.co.uk/list?list_id=938&list=gecko)
- flypig's Gecko log: [Day 91](https://www.flypig.co.uk/list?list_id=937&list=gecko)

#### Ubuntu Touch
- UBports News: [UBports Newsletter Nov 27th](http://ubports.com/blog/ubports-news-1/post/ubports-newsletter-nov-27th-3907)
- Ubuntu Touch Forums News: [Ubuntu Touch Q&amp;A 129 Saturday 2nd Dec at 19:00 UTC](https://forums.ubports.com/topic/9626/ubuntu-touch-q-a-129-saturday-2nd-dec-at-19-00-utc) _See the 
- Ubuntu Touch Forums News: [Ubuntu Touch Install and Troubleshooting](https://forums.ubports.com/topic/9624/ubuntu-touch-install-and-troubleshooting)
- rubenlcarneiro on twitter: [Lenovo M10 Hd 2nd Gen working just fine with wireless display and also with waydroid on #UbuntuTouch. @UBports  @ubuntuportugal.](https://nitter.1d4.us/rubenlcarneiro/status/1730608635976241226#m)

#### Nemo Mobile
- Nemo Mobile UX team: [Nemomobile in November 2023](https://nemomobile.net/pages/nemomobile-in-november-2023/)
  - Jozef Mlich: [Nemomobile in November 2023](https://blog.mlich.cz/2023/12/nemomobile-in-november-2023/)
- neochapay on twitter: [Qt6 branch of #nemomobile now merged into master](https://nitter.1d4.us/neochapay/status/1729502912538100024#m)

#### Distributions
- postmarketOS blog: [How to become a Trusted Contributor](https://postmarketos.org/blog/2023/12/03/how-to-become-tc/)
- postmarketOS pmaports issues: [v23.12 Infrastructure](https://gitlab.com/postmarketOS/pmaports/-/issues/2395), [Test v23.12 on main and community (#2409)](https://gitlab.com/postmarketOS/pmaports/-/issues/2409). _One week of testing!_

#### Apps
- LinuxPhoneApps.org: Apps: [Saber: Handwritten Notes](https://linuxphoneapps.org/apps/com.adilhanney.saber/)
- [colin: "a #sxmo user put together this dmenu-driven Youtube client and it actually works stupidly well"](https://fed.uninsane.org/notice/AcIiPd85YtTt3UI4iO)

#### Kernel
- phone-devel: [[PATCH v3 0/5] input/touchscreen: imagis: add support for IST3032C](http://lore.kernel.org/phone-devel/20231202125948.10345-1-karelb@gimli.ms.mff.cuni.cz/)
- phone-devel: [[PATCH v3 0/3] Enable venus on Fairphone 5 / non-ChromeOS sc7280 venus support](http://lore.kernel.org/phone-devel/20231201-sc7280-venus-pas-v3-0-bc132dc5fc30@fairphone.com/)

#### Stack
- Phoronix: [Red Hat Developing New xwayland-run & wlheadless-run Utilities](https://www.phoronix.com/news/xwayland-run)
- Phoronix: [Vulkan 1.3.272 Published With Two New Extensions](https://www.phoronix.com/news/Vulkan-1.3.272-Released)
- Phoronix: [Servo Browser Engine Continues On Its Path To Be Embed-Friendly](https://www.phoronix.com/news/Servo-November-2023)
- Phoronix: [Mesa 23.3 Released With Initial NVK Vulkan Driver, AMD RDNA3 Refresh & Raspberry Pi 5](https://www.phoronix.com/news/Mesa-23.3-Released)
- Phoronix: [Mesa 24.0 PVR Vulkan Driver Adds Support For New PowerVR Kernel Driver](https://www.phoronix.com/news/Mesa-24.0-PVR-Vulkan-Kernel)

#### Matrix
- Matrix.org: [This Week in Matrix 2023-12-01](https://matrix.org/blog/2023/12/01/this-week-in-matrix-2023-12-01/)
- Matrix.org: [Matrix v1.9 release](https://matrix.org/blog/2023/11/29/matrix-v1.9-release/)
- Matrix.org: [Shutting down the Matrix bridge to Libera Chat](https://matrix.org/blog/2023/11/28/shutting-down-bridge-to-libera-chat/). _Looks like I'll need to set up "real IRC" again, sigh."

### Worth Noting
- [justsoup: "So, bad news. #Lomiri might ta…" - Mastodon 🐘](https://mstdn.social/@justsoup/111518564461772833)
- [user0: I have made a repo for my code on Codeberg: Mobile-Friendly-Firefox..."](https://fedia.io/m/linuxphones@kbin.social/p/234473). _Awesome!_
- [Yann Büchau: ":nixos: #nix on #SailfishOS! 🥳…"](https://fosstodon.org/@nobodyinperson/111510458430760014)
- [Matv1: "This is really cool: This is really cool: @fredldotme just landed an initial release for #snap packages support in #ubuntutouch .…"](https://mastodon.social/@matv1/111505413855824007)
- [Martijn Braam: "Pushed some documentation for libdng now :) …"](https://fosstodon.org/@martijnbraam/111502535602250432)
- [David Llewellyn-Jones: "The #Linux on #Mobile stand will be at #FOSDEM once again this year!…"](https://mastodon.social/@flypig/111500745148302523)
- [LiveG Technologies: "Soon after, we made our release of #LiveGOS V0.2.0 to several new devices, including the #PinePhone and #RaspberryPi 4. …"](https://mastodon.social/@liveg/111498488454060942)
- Purism community: [Protonmail And Geary](https://forums.puri.sm/t/protonmail-and-geary/21942)
- postmarketOS pmaports issues: [LXQt Mobile UI](https://gitlab.com/postmarketOS/pmaports/-/issues/2406)
- r/MobileLinux: [Building on my own: Can somebody help me in porting Fedora Phosh on Postmarket OS to replace Alpine on my Oneplus 6?](https://www.reddit.com/r/mobilelinux/comments/1867xn4/building_on_my_own_can_somebody_help_me_in/)
- PinePhone (Reddit): [Has anyone made a RCS messaging client for Linux?](https://www.reddit.com/r/pinephone/comments/18616e4/has_anyone_made_a_rcs_messaging_client_for_linux/)
- PINE64official (Reddit): [The disappearance of community updates is really damaging.](https://www.reddit.com/r/PINE64official/comments/1855a8p/the_disappearance_of_community_updates_is_really/)
- @linmob@fosstodon.org: [Here are three additional &quot;Snows&quot; shot closer (within less than 5 minutes) together time-wise on 1. the PinePhone with megapixels2. the PinePhone Pro with GNOME Camera (snapshot) and3. the Librem 5 with millipixels (see alt texts if you are not sure which device produced which jpg).I think I somehow like the OG PinePhone's interpretation best.](https://fosstodon.org/@linmob/111510846900873085)
- @linmob@fosstodon.org: [Got a #37C3 ticket, and applied for a #LinuxMobile assembly - nothing fancy, just a small place to hang-out 🙂](https://fosstodon.org/@linmob/111495767097261532)

### Worth Reading
- Scientiac: [Scientiac](https://scientiac.space/blog/droidian/)
- etbe: [PinePhone Status](https://etbe.coker.com.au/2023/10/11/pinephone-status/). _This is about the PinePhone Pro, missed it when it came out._
- jistr.com: [Fedora on PineTab2](https://www.jistr.com/blog/2023-11-27-fedora-on-pinetab2/)
- LINux on MOBile: [Enter Beepy, Esc](https://linmob.net/enter-beepy-esc/)
- Purism community: [Librem 5 Fatigue](https://forums.puri.sm/t/librem-5-fatigue/21934)
- Purism: [Purism Leads in USA-Made Phones](https://puri.sm/posts/purism-leads-in-usa-made-phones/)

### Worth Watching
- UBports: [UBports Q&A 129](https://www.youtube.com/watch?v=-pK_TeJwxkE)
- Purism: [Purism Leads in USA-Made Phones](https://www.youtube.com/watch?v=TYRIs5cMJmc)
- Continuum Gaming: [Continuum Gaming E393: Jolla Ltd. becomes Jollaboys Ltd and Seafarix Ltd.](https://www.youtube.com/watch?v=evFS3nfepdw)
- lencho hacks: [Ubuntu Touch 20.04 OTA-3 codename should be: SPEED #ubuntutouch#Ubuntu#linuxphone #LinuxMobile](https://www.youtube.com/watch?v=o4v3f9mPN8Q)
- GojoYT: [trying to Patch kernel for samsung a20e for ubuntu touch gsi (again because i did screw up smth)](https://www.youtube.com/watch?v=SS5C5iP--y8)
- kledgeb: [【第84話】2023年11月前半のLinuxニュース・LXQt 1.4のリリースやUbuntu Touch OTA-3のリリースなど](https://www.youtube.com/watch?v=-tBkLAMFmJ0)
- Janus Cycle: [Running on a 486 CPU : Nokia's 1998 Smartphone](https://www.youtube.com/watch?v=vKb0ZmFKq3w)
- GNOME Asia Summit 2023 featured some #LinuxMobile-y lightning talks later today: [Contributing to Phosh as a Startup: A case study of Making some Impact on the Open Source Community](https://events.gnome.org/event/170/contributions/562/) and [Sustainable computing using mobile linux](https://events.gnome.org/event/170/contributions/563/) - they should be part of this [YouTube Stream](https://www.youtube.com/watch?v=yUzYnesdHeM). 

### Thanks
Huge thanks to Plata for [the nifty set of Python scripts](https://framagit.org/linmob/linmob.frama.io/-/merge_requests/5) that speed up collecting links from feeds by a lot.

### Something missing? Want to contribute?
If your project's cool story (or your awesome video or nifty blog post or ...) is missing and you don't want that to happen again, please just put it into [the hedgedoc pad](https://pad.hacc.space/7yCLy5a9QyOLWusIFiTt9A?edit) for the next one! Since I am collecting many things there, this get's you early access if you will ;-) __If you just stumble on a thing, please put it in there too - all help is appreciated!__

