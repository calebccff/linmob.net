+++
title = "Weekly GNU-like Mobile Linux Update (9/2023): Phosh 0.25.0, a first service pack for postmarketOS 22.12, and new modem firmware!"
draft = false
date = "2023-03-05T23:05:00Z"
[taxonomies]
tags = ["postmarketOS","Phosh","Ubuntu Touch","PinePhone Modem Firmware","MauiKit",]
categories = ["weekly update"]
authors = ["peter"]
[extra]
author_extra = " (with friendly assistance from plata's awesome script)"
+++

Also: Another PINE64 Community Update summing up progress and informing us of PineTab2 pricing,

<!-- more -->
_Commentary in italics._

### Software progress

#### GNOME ecosystem
- This Week in GNOME: [#85 Preferred Installations](https://thisweek.gnome.org/posts/2023/03/twig-85/)
- feborg.es: [GSoC 2023: GNOME Foundation has been accepted as a mentoring org!](https://feborg.es/gsoc-2023-gnome-foundation-has-been-accepted-as-a-mentoring-org/)
- Phoronix: [GNOME's Mutter Lands Experimental Code For HDR Modes](https://www.phoronix.com/news/Mutter-Experimental-HDR-Modes)
- Phoronix: [GNOME Shell & Mutter Complete Their Migration Away From GTK3](https://www.phoronix.com/news/GNOME-Shell-Mutter-No-GTK3)
- Phosh.mobi: [Phosh 0.25.0](https://phosh.mobi/releases/rel-0.25.0/). _Lot's of nice improvements, starting with a visual refresh for Quick Settings, but also  including more usable lock screen widgets thanks to Mobile Settings!_

#### Plasma ecosystem	
- Nate Graham: [This week in KDE: Plasma 6 begins](https://pointieststick.com/2023/03/03/this-week-in-kde-plasma-6-begins/)
- Qt Blog: [What’s new for QML Modules in 6.5](https://www.qt.io/blog/whats-new-for-qml-modules-in-6.5)
- MauiKit: [Maui Release Briefing # 1](https://mauikit.org/blog/maui-project-release-report-1/)
- KDE Announcements: [KDE Gear 22.12.3](https://kde.org/announcements/gear/22.12.3/)
- KDE Announcements: [KDE Plasma 5.27.2, Bugfix Release for February](https://kde.org/announcements/plasma/5/5.27.2/)
- Volker Krause: [OSM Hack Weekend Karlsruhe 2023](https://www.volkerkrause.eu/2023/03/04/osm-hack-weekend-karlsruhe-february-2023.html)

#### Ubuntu Touch
- UBports News: [The Post-Fosdem Newsletter edition!](http://ubports.com/blog/ubports-news-1/post/the-post-fosdem-newsletter-edition-3884). _This newsletter is definitely worth your time, as it's not only a summary of what happened in Ubuntu Touch, but also offers a glimpse into the future beyond 20.04!_


#### Distributions
- postmarketOS Blog: [v22.12 SP1: The One With A Photo Of A Librem 5 Taking A Photo Of A Librem 5 Taking A Photo](https://postmarketos.org/blog/2023/02/26/v22.12.1-release/)
- Breaking updates in pmOS edge: [Changes to camera apps installation and packaging](https://postmarketos.org/edge/2023/03/01/default-camera/)
- Breaking updates in pmOS edge: [Bugs related to pbsplash](https://postmarketos.org/edge/2023/02/26/pbsplash/)
- Manjaro Blog: [February 2023 in Manjaro ARM](https://blog.manjaro.org/february-2023-in-manjaro-arm/)

#### Linux
- Phoronix: [New ARM/RISC-V SoC Power Management Drivers Arrive For Linux 6.3](https://www.phoronix.com/news/SoC-Drivers-Linux-6.3)

#### Non-Linux
- Lup Yuen: [NuttX RTOS for PinePhone: Exploring USB](https://lupyuen.github.io/articles/usb2?88#appendix-enhanced-host-controller-interface-for-usb)

#### Matrix
- Matrix.org: [This Week in Matrix 2023-03-03](https://matrix.org/blog/2023/03/03/this-week-in-matrix-2023-03-03)
- Matrix.org: [Synapse 1.78 released](https://matrix.org/blog/2023/02/28/synapse-1-78-released)

#### Modem Firmware 
- biktorgj Modem Firmware: [0.7.4: Dirty Patching (let's try again...)](https://github.com/the-modem-distro/pinephone_modem_sdk/releases/tag/0.7.4)
- biktorgj Modem Firmware: [0.7.3: Define dancing (Testing!)](https://github.com/the-modem-distro/pinephone_modem_sdk/releases/tag/0.7.3)

### Worth noting
- [With this MR merged in systemd, it seems we'll have GNOME Clocks working with Alarms!](https://github.com/systemd/systemd/pull/26548) 
- Mobian Wiki: [organicmaps - Add -es Option](https://wiki.mobian-project.org/doku.php?id=organicmaps&rev=1677957033&do=diff)
- [Giving Voice to the Future: Support OpenVoiceOS in establishing a non-profit association.](https://ai-x.one/dev/giving-voice-to-the-future-support-openvoiceos-in-establishing-a-non-profit-association/)
- [LinuxPhoneApps.org: "Sad news: Two apps had to be marked as archived…"](https://fosstodon.org/@linuxphoneapps@linuxrocks.online/109966500936331880)

### Worth reading
- PINE64: [February update: things are taking shape](https://www.pine64.org/2023/03/01/february-update-things-are-taking-shape/)
  - PINE64official (Reddit): [February update: things are taking shape | PINE64](https://www.reddit.com/r/PINE64official/comments/11fa7n2/february_update_things_are_taking_shape_pine64/)
- Purism: [My Top 10 Lapdock Kit Tips](https://puri.sm/posts/my-top-10-lapdock-kit-tips/)
  - Purism community: [New Post: My Top 10 Lapdock Kit Tips](https://forums.puri.sm/t/new-post-my-top-10-lapdock-kit-tips/19580)
- Liliputing: [PineTab2 Linux tablet will sell for $159 and up](https://liliputing.com/pinetab2-linux-tablet-will-sell-for-159-and-up/)
- Martijn Braam: [Sensors and PCB design](https://blog.brixit.nl/sensors-and-pcb-design/)
- Purism (Reddit): [The good, The Bad, and The Ugly: A few months w/ Librem 5 *WARNING* - this is a book.](https://www.reddit.com/r/Purism/comments/11ho5xo/the_good_the_bad_and_the_ugly_a_few_months_w/)

### Worth watching
- PINE64: [February Update: Things Are Taking Shape](https://www.youtube.com/watch?v=rN1gv1ttJAs)
- FOSSfrog: [A simple Rucky like app for PinePhone](https://www.youtube.com/watch?v=DGy53R0opbo)
- qkali: [Pinephone Pro - February 2023](https://www.youtube.com/watch?v=nYz1JPP9mO8) _Sxmo on DanctNIX, everybody!_
- Continuum Gaming: [Microsoft Continuum Gaming E353: Sailfish OS Micro G – What is it and how to get it?](https://www.youtube.com/watch?v=tedUrd_8PuU)
- MrNicetux: [[#Linux #Jolla #European smartphone OS] Sailfisch x86_64 port on a notebook (German, no sound)](https://www.youtube.com/watch?v=a2sWKBw1_5I)
- Timur Moziev: [postmarketOS on Samsung Galaxy Note 10.1 (GT-N8000) Test Run](https://www.youtube.com/watch?v=WcWIYvnAdhE)
- neochapay: [fixed pulse volume control on nemomobile](https://www.youtube.com/shorts/jA7yiqHapGg)



### Thanks
Huge thanks again to Plata for [the nifty set of Python scripts](https://framagit.org/linmob/linmob.frama.io/-/merge_requests/5) that speeds up collecting links from feeds by a lot.

### Something missing? Want to contribute?
If your project's cool story (or your awesome video or nifty blog post or ...) is missing and you don't want that to happen again, please just put it into [the hedgedoc pad](https://pad.hacc.space/7yCLy5a9QyOLWusIFiTt9A?edit) for the next one! Since I am collecting many things there, this get's you early access if you will ;-) __If you just stumble on a thing, please put it in there too - all help is appreciated!__



