+++
title = "LinBits 8: Weekly PinePhone news / media roundup (week 35)"
aliases = ["2020/08/30/linbits8-weekly-pinephone-news-week35.html", "linbits8"]
author = "Peter"
date = "2020-08-30T12:50:00Z"
layout = "post"
[taxonomies]
authors = ["peter"]
tags = ["Pine64", "PinePhone", "Ubuntu Touch", "UBports Q&A", "Arch Linux ARM", "Anbox", "adb", "App Lists", "Purism", "Librem 5", "Librem 5 Evergreen", "SailfishOS", "Nemo Mobile", "Nexus 5", "postmarketOS", "Fairphone", "AppDirectory"]
categories = ["weekly update"]
+++

_It's sunday. Now what happened since last sunday?_

Anbox improvements, a ton of Purism news, an incrementally better Fairphone, App Lists and much more. _Commentary in italics._
<!-- more -->
### Software: releases and improvements
* [Huong Tram Linux/Arch Linux ARM](https://github.com/dreemurrs-embedded/Pine64-Arch/releases) have updated their `anbox` and `anbox-image` package, enabling networking, preinstalled F-Droid and a visible Android keyboard. _This makes Android apps a __lot__ more usable. Also, ADB has been fixed on Arch, which helps with installing apps and other things._
* [Mobian debugged ModemManager issues and can run continuous integration now](https://fosstodon.org/@mobian/104778243987170547).

### Worth reading

* Purism: [3D Gaming on the Librem 5](https://puri.sm/posts/3d-gaming-on-the-librem-5/). _This is actually looking surprisingly good._
* Purism: [The design behind a modular and secure mobile phone](https://puri.sm/posts/the-design-behind-a-modular-and-secure-mobile-phone/). _I was not sure whether I should include this, as it feels like an ad._
* Plasma Mobile: [Plasma Mobile update: May-August 2020](https://www.plasma-mobile.org/2020/08/25/plasma-mobile-update-august.html). _Great progress at Plasma Mobile._
* eighty-twenty: [Squeak Smalltalk on a PostmarketOS cellphone](https://eighty-twenty.org/2020/08/25/postmarketos-smalltalk). _This is not on a PinePhone, but a Samsung Galaxy S7, but it's quite interesting still._
* SailfishOS Forums: [OBS shut down and next steps](https://forum.sailfishos.org/t/obs-shut-down-and-next-steps/1814). _The saga continues. All these changes by Jolla have not been received well by the SFOS porters community – just see the next item. It really looks like we might see less community involvement with Jollas OS in the future. This shutdown of services may have an upside though, as a switch to openSUSE Build Service is likely to help projects like [NemoMobile](https://nemomobile.net) with swtiching to newer versions of Qt (Sailfish OS is staying on Qt 5.6 forever for licensing reasons, as they remain unwilling to open source the proprietary components of their UI)._
* Dylan van Assche: [End of an era](https://dylanvanassche.be/blog/2020/end-of-an-era/). _Dylan is not the only [one](https://twitter.com/eugenio_g7/status/1297162722023440384) leaving the Sailfish OS community._ 
* Amos B. Batto: [Comparing specs of Linux phones](https://amosbbatto.wordpress.com/2020/08/25/comparing-linux-phones/). _Nice list, although it lacks some of the better supported postmarketOS devices (by limiting to devices that are sold with Linux OS on them) and old, but still community-supported devices like the Nokia N900._
* Purism: [Librem 5 Evergreen Update: Mold and Milestones](https://puri.sm/posts/librem-5-evergreen-update-mold-and-milestones/). _Purism sharing another progress report. I am really curious if I am going to receive my Librem 5 this year._

### Worth watching

* nas: [Mainstream Apps on the Pinephone](https://peertube.co.uk/videos/watch/a368a023-ce44-44f0-a42d-f8880dc65529). _That's not Anbox, it's the current GloDroid release._
* debconf20, Guido Günther: [My phone runs debian &mdash; and it does phone calls](https://meetings-archive.debian.net/pub/debian-meetings/2020/DebConf20/13-my-phone-runs-debian-and-it-does-phone-calls.webm). _It's an interesting talk. [Slides](https://git.sigxcpu.org/cgit/talks/2020-debconf-mobile/plain/talk.pdf)._
* LinuxPlumbersConf: [Bushan Shah: Plasma on Mobile devices](https://youtu.be/4RZhTRZYb8I?t=6309). _If you care about Plasma Mobile, but are not yet too familiar with the project, you may find this talk quite informative. [Slides](https://linuxplumbersconf.org/event/7/contributions/835/attachments/692/1278/plamo-lpc-2020-applications-ecosystem-mc.pdf)._
* dos: [Vivante GC7000Lite vs. Mali-400 MP2 (Mesa 20.1.5, Linux 5.7)](https://social.librem.one/@dos/104767475144787918). _What's better than benchmarks? Real gaming performance._
* TuxPhones: [Experiment: GNOME Shell on the Nexus 5 (postmarketOS + mainline Linux)](https://www.youtube.com/watch?v=hdOjS1cNCbU). _I would have loved to see the graphical glitches here, editing them out kind of defeats the purpose of the video for me._
* UBports: [Ubuntu Touch Q&A 83](https://www.youtube.com/watch?v=50Ar0aJuGWk). _This time it's all about OTA-13, which will bring many more ARM64 devices up._
* DanctNIX: [PinePhone and Redmi 4X - Boot Time Test](https://youtu.be/1qDGO_2ZNiI). _Yes, the PinePhone is quite fast at booting._
* Howto: Privacy and Infosec: [Howto: Tshark Sniffer Analyzer: Pinephone: PureOS (Linux)](https://youtu.be/I5JfByiY6QY). 

### Something completely different
* The Verge: [You can buy Fairphone’s new handset or just its cameras as an upgrade](https://www.theverge.com/2020/8/27/21375326/fairphone-3-plus-release-date-news-features-cameras-ethical-sustainable). _I know, the Fairphone 3+ does not run any of our favourite GNU/Linux based operating systems yet, neither is it super affordable. But the fact that it is ethically sourced and Fairphone just iterated on the cameras, and makes the new cameras available to everybody who has the Fairphone 3 is just great in my book. Also, the Fairphone 2 still gets software updates (and is supported by LineageOS and UBports), which is exceptional for a 2015 phone running a rare chipset._

### Stuff I did
* I set up a [fork](https://linmobapps.frama.io/) of the [MGLapps list](https://mglapps.frama.io), then had second thoughts about forking and have started work with _cahfofpai_, author/maintainer of the MGLapps on a new thing called _app directory_ around Hugo and other components, enabling ways to contribute for users too. _Stay tuned for the announcement, due hopefully soon._

