+++
title = "Weekly Update (1-2/2022): PinePhone Pro and accessories shipping, Poco F1 call audio progress, libadwaita 1.0, and modem firmware updating"
date = "2022-01-14T19:40:00Z"
updated = "2022-01-15T21:52:00Z"
draft = false
[taxonomies]
tags = ["PinePhone","PinePhone Pro","PinePhone accessories","postmarketOS","libadwaita","Maui","Purism","modem firmware",]
categories = ["weekly update"]
authors = ["peter"]
[extra]
update_note = "Added comment to PineNote videos, two UBports stories and Sxmo 1.7.0 and PinePhone Keyboard related blog posts I somehow missed originally."
+++

_A new year begins with exciting news regarding Linux Phones:_ Experimental call audio on Snapdragon 845 devices, better PinePhone modem updating for postmarketOS (Edge), PinePhone accesories and PinePhone Pro up for order, Ubuntu Touch OTA 21, Sxmo 1.7.0 and more! Also: This weekly update has a new name!
<!-- more --> _Commentary in italics._ 


### Editorial: A new start, a slightly different format

At the end of 2021, I felt increasingly bored and tired by writing all these weekly updates. While my process of going through things using an RSS agregator is not too bad, the process of manually copy-pasting all these URLs, names and titles into an unordered list of Markdown links felt more and more like bad busywork. To be frank: There have been many weeks where I dreaded my Wednesday evening. I did not want to go through tons of irrelevant content to make sure I wouldn't miss the interesting tidbits any more.

But: Writing regular little updates is easier to do for me than editing videos, and enough people told me how much they like these updates. So I've decided to carry on, but due to less time[^1], I will exclude duplicate posts that don't add much value. To ease this work for me, just make sure [to ping me](#something-missing) if I don't include your cool project all the time anyway! Going forward, I want to change hosting of this site to a public Git repo again (likely framagit.org), to make merge requests or even collaborative editing possible.



### Hardware news
* The [PinePhone Pro Explorer Edition is now available for purchase.](https://www.pine64.org/2022/01/11/pinephone-pro-explorer-edition-pre-orders-open-january-11/). If you understand, that this device is not ready yet as hardware enablement is not fully done (see [also](https://fosstodon.org/@linmob/107605215463739200)), go get one. _I ordered one._
* Also, PINE64 have [finally made the PinePhone accessories available](https://www.pine64.org/2021/12/31/happy-new-year-the-keyboard-and-cases-are-here/) (the keyboard[^2] and various backcovers: LoRa, Wireless Charging and Fingerprint sensor) , shortly after LinBits 77. I've ordered all the things and received them yesterday but have not really tried them out extensively.[^3] [Read some PinePhone keyboard first impressions below](#pinephone-keyboard-first-impressions-and-fixes).

### Hardware enablement news
* The [Xiaomi Poco F1 is about to get support for audio calls soon-ish](https://twitter.com/joelselvaraj95/status/1478971761127350275)! _Looks like Snapdragon 845 phones are going to be fully viable mainline Linux phones relatively soon (soon as in later this year)._



### Software news
#### GNOME ecosystem
* This Week in GNOME: [#25 The Big 1.0](https://thisweek.gnome.org/posts/2022/01/twig-25/).
* This Week in GNOME: [#26 Contact Me](https://thisweek.gnome.org/posts/2022/01/twig-26/). _Contacts and Secret sound very promising! And boy do I look forward to alphanumeric passwords for Phosh!_
* Alexander Mikhaylenko: [Libadwaita 1.0](https://blogs.gnome.org/alexm/2021/12/31/libadwaita-1-0/)_Libadwaita 1.0 is a big deal, for packaging in distributions alone. This should increase distro adoption of GTK4/Libadwaita applications, many of which running fine on Linux Phones._
* Allan Day: [Human Interface Guidelines, libadwaita 1.0 edition](https://blogs.gnome.org/aday/2022/01/13/human-interface-guidelines-libadwaita-1-0-edition/).

#### Plasma/Maui ecosystem
* Maui Project: [Maui Report 17](https://mauikit.org/blog/maui-report-17/).
* Volker Krause: [November/December in KDE PIM](https://www.volkerkrause.eu/2022/01/08/kde-pim-november-december-2021.html).

#### UBports Ubuntu Touch
* Dalton Durst for UBports: [Ubuntu Touch OTA-21 Release](http://ubports.com/blog/ubports-news-1/post/ubuntu-touch-ota-21-release-3798). _While this release is "just" for the non-mainline devices (not for PinePhone or PineTab), it's great to see so much feature work continuing while the transition to an Ubuntu 20.04 base is happening._

#### Sxmo 1.70
Sxmo 1.7.0 is out, delivering multiple improvements including visual voicemail support. Make sure to [read the announcement](https://lists.sr.ht/~mil/sxmo-announce/%3C20220107164241.u7q5uaok5wkw37y6%40worker.anaproy.lxd%3E) for more details.

#### Others
* Nemo mobile: [Put your hand to the work on Nemomobile](https://nemomobile.net/pages/put-your-hand-to-the-work-on-nemomobile/). _A bug list, so if contributing more is on your list of new year resolutions, you can get to work here._



### Worth reading

#### Kernel work
* Phoronix: [Hantro Media Driver Adds VP9 Acceleration With Linux 5.17](https://www.phoronix.com/scan.php?page=news_item&px=Hantro-VP9-Linux-5.17). _This is relevant for both the Librem 5 and the PinePhone Pro._

#### postmarketOS awesomeness
* Martijn Braam: [Why installers are needed for disk encryption](https://blog.brixit.nl/why-installers-are-needed-for-fde/). _Great explainer. I hope that more distributions adopt similar installers._
* TuxPhones: [You can now live-boot postmarketOS on Android phones](https://tuxphones.com/postmarketos-linux-live-usb-fastboot-network-boot/).
* TuxPhones: [Automating the little things](https://tuxphones.com/draft-automating-the-little-things-might-actually-be-a-bad-idea/).

#### Firmware management
* Dylan van Assche: [Upgrading your PinePhone modem easily](https://dylanvanassche.be/blog/2022/pinephone-modem-upgrade/). _This is huge news, and a great way to solve the modem firmware conundrum: PINE64 can't ship the open source firmare, which is a security risk. Dylan's approach does not reinvent the wheel, but works with minor changes to existing, established projects - this is going to be great once it lands!_
  * Phoronix: [Fwupd 1.7.4 Supports More Hardware For Firmware Updating On Linux](https://www.phoronix.com/scan.php?page=news_item&px=Fwupd-1.7.4-Released). _Quectel EG25-G is in there!_

#### Long Term Support
* UBports: [Ubuntu Touch and the 10 Year Smartphone](http://ubports.com/blog/ubports-news-1/post/ubuntu-touch-and-the-10-year-smartphone-3799). _The idea of keeping devices usable (and at least somewhat secure to use) for really long time frames is something that drives my excitement about Linux Phones, as I believe the "every two years a new phone" model is not sustainable at all. \*Pretends he does not own way too many smartphones.\*_

#### Purism news
* Purism: [2021 Year in Review: Design](https://puri.sm/posts/2021-year-in-review-design/).
* Purism: [Throwback to 2021, More from Librem 5 in 2022](https://puri.sm/posts/throwback-to-2021-more-from-librem-5-in-2022/).

#### PinePhone keyboard first impressions (and fixes)
* Avery: [PinePhone Keyboard First Impressions](https://avery.cafe/blog/pinephone-keyboard-first-impressions/).
* Avery: [A Quick Fix for the PinePhone Keyboard's Top Row](https://avery.cafe/blog/a-quick-fix-for-the-pinephone-keyboards-top-row/). _It's fine for me on my unit, but if it's you have problems, this might help!_

### Worth watching

#### Crazy attempts to run Linux
* Thomas Triadi: [JingOS Arm64 port running on Xiaomi Redmi Note 4x ( xiaomi mido )](https://www.youtube.com/watch?v=a3Doy8HzTko). _A postmarketOS kernel, an Ubuntu rootfs, and JingOS Software. As [@tuxdevices points out](https://fosstodon.org/@tuxdevices/107571057163406522), this is not too fast, but it's still interesting._
* Janus Cycle: [Linux on the Windows 7 Phone](https://www.youtube.com/watch?v=jd1dpUUXrsw). _The Fujitsu F-07C phone in this video actually runs Windows 7 Home Premium by default and is about 10 years old, using an Intel Atom Lincroft chip._

#### PinePhone Unboxing and new user thoughts
* Nick Shulhin: [I bought a PinePhone | Linux Mobile phone](https://www.youtube.com/watch?v=Qv3JGZsMwgY). _Nice unboxing._

#### PinePhone tutorials
* Avisando: [How to flash an OS on the PinePhone without a PC (eMMC or SD)](https://www.youtube.com/watch?v=bgAnQKCYxQY). _Step by step. I waguely recall doing a similar video about a year ago._

#### Ubuntu Touch
* Nick Shulhin: [I love Ubuntu Touch! | PinePhone | Linux](https://www.youtube.com/watch?v=0yZb7g5o97A). _As I commented: Don't run that ancient stable channel on your PinePhone, try something newer!_
* Linux Lounge: [SuperTux on Ubuntu Touch! - Ubuntu Touch Game Review](https://www.youtube.com/watch?v=ujJnlsa06ZU).

#### XMPP fun
* RTP Privacy & Tech Tips: [Dino (XMPP): Decentralized End To End Encryption Messenger/+ Chatroom Client (Linux/Pinephone Shown)](https://tube.tchncs.de/w/86d8b6df-8000-41f6-a92c-1d8ba2501972). _Please note: You don't have to run your PinePhone at 1x scaling to use Dino, just build and run the [libhandy-enabled branch](https://github.com/dino/dino/tree/feature/handy) - postmarketOS even ships it, and there are relatively easy ways to install it on [Mobian](https://wiki.mobian-project.org/doku.php?id=dino&s[]=dino) and [Arch](https://framagit.org/linmobapps/pkgbuilds/-/tree/main/dino-mobile-git)._

#### My eInk runs Arch, BTW
* Danct12: [Phosh UI running on PineNote](https://www.youtube.com/watch?v=76uFrG3b-Y0).
* Danct12: [PineNote booting Arch Linux ARM](https://www.youtube.com/watch?v=qC5ynJENo0Y). _For context: The eInk screen of the PineNote [is now working with mainline Linux](https://fosstodon.org/@PINE64/107566967950259308), which is just awesome!_

#### Development Streams
* caleb: [OnePlus 6 camera shenanigans pt 2 #Linux #postmarketOS](https://www.youtube.com/watch?v=0MJpcKvIcIs).
* caleb: [OnePlus 6 camera shenanigans pt 3 #Linux #postmarketOS](https://www.youtube.com/watch?v=-mp3lmVcXjo).

#### JingOS progress
* JingOS: [[Preview] JingOS V1.2 ARM - Screen Rotation - Fingerprint Unlock - JingPad A1](https://www.youtube.com/watch?v=Yw0iW3EtE7E). _Flatpak support is a big deal._

### Something missing?

If your projects' cool story is missing and you don't want that to happen again, please get in touch via social media or email!

[^1]: New dayjob!

[^2]: The first keyboards shipped without a manual, if you're affected by this, [here's the PDF](https://wiki.pine64.org/images/1/12/USER_MANUAL-KEYBOARD-V2-EN-DE-FR-ES.pdf).

[^3]: I attempted an unboxing live stream, but had serious set-up woes. Firstly, Plasma's Wayland session is apparently not ready for OBS Streaming, and after that I messed with too many settings so that my video looked terribly (which I, due to a small stream preview window, only noticed after ending the second attempt to stream (on Plasma X11)[.](https://youtu.be/l7kWqNhaEw0) To complete a full blown train wreck, the Keyboard as the only accessory I tried on camera, did not work with my postmarketOS CE running DanctNIX - luckily it works with the borked Plasma Mobile install on my UBports CE PinePhone. __Update, January 15th, 2022:__ After cleaning the pogo pins, it works just fine with DanctNIX on my 3GB PinePhone, too – if only Phosh were better in landscape mode.
