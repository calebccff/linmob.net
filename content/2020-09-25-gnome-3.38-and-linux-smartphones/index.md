+++
title = "GNOME 3.38 and what it brings Linux Smartphones"
aliases = ["2020/09/25/gnome-3.38-and-linux-smartphones.html"]
author = "Peter"
date = "2020-09-25T00:30:00Z"
layout = "post"
[taxonomies]
tags = ["PinePhone", "Gnome on Mobile", "Phosh", "Arch Linux ARM", "AUR", "Games", "Apps", "GNOME Apps", "Navigation", "Browsers"]
categories = ["software"]
authors = ["peter"]
+++
_There is one more item I forgot to add while writing LinBits 11:_ [The release notes of GNOME 3.38 "Orbis"](https://help.gnome.org/misc/release-notes/3.38/). I tried to have a look at it on [Arch Linux ARM/Huong Tram Linux by danct12](https://github.com/dreemurrs-embedded/Pine64-Arch/releases), and I am going to share the progress here, hopefully allowing you to follow along.
<!-- more -->

## Preparations

Please follow the [preparations section of my recent post on building software from the AUR](https://linmob.net/2020/09/05/pinephone-building-plasma-mobile-apps-from-the-aur.html#preparations).

## Installing software from the AUR

### GNOME Web/Epiphany

I thought about building the full browser here, my attempt at building `epiphany-git` eventually failed miserably, and building `webkit2gtk`  before is a necessity &mdash; which takes forever. If you want to, you can try the new release of Epiphany via flatpak. Most of it scales properly, but the Settings screen is not phone friendly yet (it can made somewhat usable with `scale-to-fit epiphany on`).

![GNOME Web: Web page with menu](20200924_22h25m03s_grim.jpg)
![GNOME Web: Tab menu](20200924_22h25m13s_grim.jpg)
![GNOME Web: Settings after scale-to-fit](20200924_22h29m34s_grim.jpg)
![GNOME Web: Shortcuts after scale-to-fit](20200924_22h30m03s_grim.jpg)

### GNOME Games

Let's try out GNOME Games. First, we will have to install `retro-gtk-git`, which is an unnamed dependency of `gnome-games-git`. If you are brave, you may do both installs at once: `yay -S retro-gtk-git gnome-games-git`.

It fits the screen quite nicely, but I did not want to download ROMs from dodgy sites so I could not really try it.

![GNOME Games: Start screen with menu](20200924_22h39m24s_grim.jpg)
![GNOME Games: Preferences: Video](20200924_22h49m47s_grim.jpg)
![GNOME Games: Preferences: Platforms](20200924_22h49m54s_grim.jpg)
![GNOME Games: Event Overview](20200924_22h50m02s_grim.jpg)

### GNOME Maps

GNOME Maps is said to have "received initial work making the app adaptive for phone use." Let's install it by running `yay -S gnome-maps-git`. 
 
The main screen is a lot better now, but the Routing menu  still needs further adjustment, which, if I understand the [commit history](https://gitlab.gnome.org/GNOME/gnome-maps/-/commits/master) correctly, might have had at some point during the development of the release.

![GNOME Maps: Map View](20200924_22h17m58s_grim.jpg)
![GNOME Maps: Dark Mode satellite view](20200924_22h14m28s_grim.jpg)
![GNOME Maps: Shortcuts](20200924_22h16m54s_grim.jpg)
![GNOME Maps: Routing view](20200924_22h18m37s_grim.jpg)


### GNOME Calculator

It's `yay -S gnome-calculator-git`, and there does not seem to be much difference except for a new icon, that well, is different. `scale-to-fit gnome-calculator on` helps with the advanced modes.

![GNOME Calculator: Icon](20200924_22h10m26s_grim.jpg)
![GNOME Calculator: Basic Mode](20200924_22h10m37s_grim.jpg)
![GNOME Calculator: Programming Mode](20200924_22h11m21s_grim.jpg)
![GNOME Calculator: Programming Mode with scale-to-fit](20200925_00h25m44s_grim.jpg)



### GNOME Clocks

Clocks has received an overhaul. It's a quick build away with `yay -S gnome-clocks-git`. I did not really notice a difference in features, but the redesign certainly improves it.


![GNOME Clocks: Start screen](20200924_22h33m32s_grim.jpg)
![GNOME Clocks: Adding a world clock](20200924_22h33m46s_grim.jpg)
![GNOME Clocks: Add an Alarm](20200924_22h34m05s_grim.jpg)
![GNOME Clocks: Timer](20200924_22h37m09s_grim.jpg)


## Caveats and Conclusion

GNOME 3.38 brings a few improvements. Maps, while it does not scale properly in every part of the application, is a lot better now. I did not really try games, because I did not feel like pirating games and installing emulators. 

For GNOME Maps, Clocks and Image Viewer (eog), you can also have a look at my [recent Manjaro video on YouTube](https://www.youtube.com/watch?v=VefFyTF3c-I).
