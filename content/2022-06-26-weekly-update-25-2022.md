+++
title = "Weekly #MobilePOSIX Update (25/2022): Better Processing in Megapixels and another report on the PinePhone Pro Cameras"
date = "2022-06-26T22:08:00Z"
draft = false
[taxonomies]
tags = ["PinePhone Pro","Camera","Megapixels","PinePhone modem firmware","Ubuntu Touch",]
categories = ["weekly update"]
authors = ["peter"]
+++

The usual development progress, another nice Q&A video by UBports and DOOM on a Modem!

<!-- more -->
_Commentary in italics._

### Software progress

#### GNOME ecosystem
* This Week in GNOME: [#49 New Views](https://thisweek.gnome.org/posts/2022/06/twig-49/). _More Files and Calendar improvements, Pika Backup working better as a Flatpak are the highlight's of GNOME's week._
* James Westman: [Labelling Maps is Surprisingly Hard](https://www.jwestman.net/2022/06/18/labelling-maps-is-surprisingly-hard.html). _Great read!_
* Georges Stavracas: [Giving up on GNOME To Do](https://feaneron.com/2022/06/21/giving-up-on-gnome-to-do/). _It's always sad when someone has to give up on a project, but may this change may help getting To Do [adaptive](https://gitlab.gnome.org/GNOME/gnome-todo/-/issues/427) outside of [Mobian's fork](https://gitlab.com/mobian1/packages/gnome-todo)._
* chergert: [Builder GTK 4 Porting, Part VII](https://blogs.gnome.org/chergert/2022/06/25/builder-gtk-4-porting-part-vii/).
* Carlos Garcia Campos: [Thread safety support in libsoup3](https://blogs.igalia.com/carlosgc/2022/06/21/thread-safety-support-in-libsoup3/).

#### Plasma/Maui ecosystem

* Nate Graham: [This week in KDE: a mad bugfixing spree](https://pointieststick.com/2022/06/24/this-week-in-kde-a-mad-bugfixing-spree/). _Nice progress!_
* Felipe Kinoshita: [My week in KDE: Pomodoro and Lorem Ipsum](https://fhek.gitlab.io/en/my-week-in-kde-pomodoro-and-lorem-ipsum/). _I'll need to check how well Pomodoro works on mobile!_
* SMIT S PATIL: [Porting KCM modules from QtWidgets to QtQuick/Kirigami for GSOC 2022](https://smit17.netlify.app/posts/gsoc22-progress-report/).
* KDE News: [KDE Apps Mid-Year Update](https://dot.kde.org/2022/06/25/kde-apps-mid-year-update). _Not much in terms of mobile in here (seemingly, there's going to be a mobile specific post on June 28th), but Haruna might be worth another look._

#### Ubuntu Touch
* Florian Leeber for UBports: [Ubuntu Touch OTA-23 Call for Testing](http://ubports.com/blog/ubports-news-1/post/ubuntu-touch-ota-23-call-for-testing-3852). _It's not too late to get involved in the testing game!_
  * 9to5linux: [Ubuntu Touch OTA-23 Is Coming on June 28th with FM Radio Expansion, Lomiri Fixes](https://9to5linux.com/ubuntu-touch-ota-23-is-coming-on-june-28th-with-fm-radio-expansion-lomiri-fixes).
* UBports: [Ubuntu Touch Q&A 118](https://ubports.com/blog/ubports-news-1/post/ubuntu-touch-q-a-118-3853). _The transscript and only audio variant of the last Q&A. Make sure to not miss Q&A 119 down below!_

#### Nemo Mobile
* Nemo Mobile: [Nemomobile in June 2022](https://nemomobile.net/pages/nemomobile-in-june-2022/). _Not much progress, but when the foundations change, that's to be expected. Make sure to watch the talk!_

#### Apps
* Megapixels has seen [another release (1.5.0)](https://gitlab.com/postmarketOS/megapixels/-/tags/1.5.0), featuring what Martijn outlined in his video below.
* [Tokodon 22.06](https://apps.kde.org/tokodon/) now features a Notification view and supports more instances. _Via [Blort](https://social.tchncs.de/@Blort), thanks!_


### Worth noting
* If you are a Librem 5 owner and have had trouble with charging, you're going to find [this thread on Purism's forums](https://forums.puri.sm/t/usb-dcp-5v-1-5a-protocol-and-librem-5/17637) helpful. _Great work, Quarnero!_
* If you've been looking for a Phosh specific settings app that helps with scale-to-fit and feedbackd, [Guido Günther has been cooking something nice](https://source.puri.sm/guido.gunther/phosh-mobile-settings/-/tree/pureos/byzantium)!
* If you've got a PinePhone Convergence Edition and have been looking for a dock, [now you can print one](https://libredd.it/r/PINE64official/comments/vj4p7k/i_created_a_stand_for_the_pinephone_and_the/)!
* If you've been wanting to run your PinePhone in 4G only mode (may work for other phones two), and have Tor ready, [RTP has something nice for you](https://www.buymeacoffee.com/politictech/smartphone-privacy-mitigation-of-false-towers-imsi-catchers-stingrays)! _I don't have Tor setup on the device I am writing this on (a Librem 5) and thus did not test it yet._
* Doom on a Modem? [Why not!](https://github.com/Biktorgj/pinephone_modem_sdk/releases/tag/0.6.7-Doom)

### Worth reading

#### PinePhone Pro Camera progress
* megi's PinePhone Development Log: [Further Pinephone Pro camera development](https://xnux.eu/log/#070). _Great work! (I must admit that the last sentence saddens me a bit though.)_
* 

#### Competition
* fossphones.com: [Linux Phone News - June 24, 2022](https://fossphones.com/06-24-22.html). _Nice roundup! Also, I am excited what comes out of [this](https://fossphones.com/pinephone-dd-challenge.html)!_

#### App news
* Günther Wagner: [Hackgregator rewritten in Rust](https://www.gwagner.dev/hackgregator-rewritten-in-rust/). _Nice writeup!_
* Alberto Mardegan: [MiTubo comes to macOS](http://mardy.it/blog/2022/06/mitubo-comes-to-macos.html).
* OMG!Ubuntu!: [Clapper GTK Video App Bags Performance Boost in Latest Update](https://www.omgubuntu.co.uk/2022/06/clapper-the-stylish-gtk-video-player-bags-performance-improvements-in-latest-update). _I did not test this on my mobile Linux devices yet (I don't watch enough videos on tiny screens), feedback welcome!_

#### App Development
* lupyuen: [Build a PinePhone App with Zig and zgt](https://lupyuen.github.io/articles/pinephone). _Nice tutorial!_

#### Tethering
* David Hamner for Purism: [How to Enable Hot Spot and Tethering in PureOS on Your Librem 5](https://puri.sm/posts/how-to-enable-hot-spot-and-tethering-in-pureos-on-your-librem-5/).

#### Tablets
* The Changelog: [I Finally Found a Solid Debian Tablet: The Surface Go 2](https://changelog.complete.org/archives/10396-i-finally-found-a-solid-debian-tablet-the-surface-go-2), [hn comments](https://news.ycombinator.com/item?id=31873440). _I may have some limited experience with a [similar device](https://fosstodon.org/@linmob/108539262002232920), more on that when I find the time._


### Worth watching

#### Ubuntu Touch
* UBports: [Ubuntu Touch Q&A 119](https://www.youtube.com/watch?v=GlKWt2YRlxA). _Great updates: Focal really seems to make progress, work on Waydroid, a first GTK3 app for the Open Store, news on Miroil and Lomiri. Still, they remain resource constrained - please help them out - e.g. hosting Q&As!_

#### Better Megapixels
* Martijn Braam: [My progress with messing with Megapixels](https://spacepub.space/videos/watch/56a99cf0-0428-45b6-a11e-ba7ce0173b7c), [YouTube](https://www.youtube.com/watch?v=Ypc3pfzSajo). _Great work by Martijn and the other contributors, I just tried the new release with the new postprocessing feature, and it's notably better!_

#### Nemo Mobile
* DevConf: [Current status of Nemomobile - DevConf.cz Mini | June 2022](https://www.youtube.com/watch?v=Ya0nSC-68OU).
* Сергей Чуплыгин: [crop widget nemomobile](https://www.youtube.com/watch?v=8pkYeqo-W1g). 

#### Tethering
* Purism: [How to Enable Hot Spot and Tethering in PureOS on Your Librem 5 (Updated)](https://www.youtube.com/watch?v=frtm5865EPU).

#### Music Apps
* baby WOGUE: [Amberol Vs G4 Music | Nothing is, how it seems :p](https://www.youtube.com/watch?v=8frbwZIJEWE). _Both these apps work great on Phones, too._

#### Adaptive Dialogues
* baby WOGUE: [New responsive dialogs, Ft GNOME 43 ..but that's not about that..](https://www.youtube.com/watch?v=gVh_qH1ODNU).

### Something missing?
If your project's cool story (or your awesome video or nifty blog post or ...) is missing and you don't want that to happen again, please get in touch via social media or email!

PS: In case you are wondering about the title [...](https://fosstodon.org/@linmob/108516506484897358)
