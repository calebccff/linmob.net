+++
title = "Weekly GNU-like Mobile Linux Update (32/2022): Summer breeze and travel reports"
date = "2022-08-14T14:03:00Z"
updated = "2022-08-16T06:15:00Z"
draft = false
[taxonomies]
tags = ["Phosh", "PINE64 Community Q&A", "PinePhone", "JingPad",]
categories = ["weekly update"]
authors = ["peter"]
[extra]
update_note = "Added Phosh 0.20."
+++
A new Sailfish OS Community Update, a PINE64 Community Q&A, and a look back at the JingPad.
<!-- more -->
_Commentary in italics._

### Software progress

#### GNOME ecosystem
* This Week in GNOME: [#56 Refined Documentation](https://thisweek.gnome.org/posts/2022/08/twig-56/). _This week with a very welcome usability enhancement to GNOME Calls!_
* Utkarsh Gandhi: [GSoC 2022: Third Update!](https://utkarsh2401.blogspot.com/2022/08/gsoc-2022-third-update.html). _Nice work on Nautilus!_

##### Releases
* Phosh 0.20 [has been released properly](https://social.librem.one/@agx/108787518473435790), and with it [some helpful tools](https://social.librem.one/@agx/108787533689154387).[^1]

#### Plasma/Maui ecosystem
* Nate Graham: [This week in KDE: Major accessibility improvements](https://pointieststick.com/2022/08/12/this-week-in-kde-major-accessibility-improvements/).
  * Nate Graham: [Next week in KDE: mentioning fewer microscopic bugfixes](https://pointieststick.com/2022/08/09/next-week-in-kde-mentioning-fewer-microscopic-bugfixes/). _Makes sense._
* Volker Krause: [KDE Frameworks 6 QML porting](https://www.volkerkrause.eu/2022/08/13/kf6-qml-porting-progress.html)

#### Ubuntu Touch
* Jeroen Baten for UBports: [Hello! Another edition of your Biweekly UBports newsletter awaits. Enjoy!](https://ubports.com/de/blog/ubports-blogs-nachrichten-1/post/hello-another-edition-of-your-biweekly-ubports-newsletter-awaits-enjoy-3859).

#### Sailfish OS
* flypig: [Sailfish Community News, 11th August, Summertime](https://forum.sailfishos.org/t/sailfish-community-news-11th-august-summertime/12623). _Jolla summer sale, and actual community news. Glad to read that hydrogen is working on Sailfish OS, too :)_


#### Kernel fun
* [Linux 6.0 Promotes Its H.265/HEVC User-Space API To Stable - Phoronix](https://www.phoronix.com/news/Linux-6.0-Media).
#### X11
* [Who-T: The new XWAYLAND extension is available](https://who-t.blogspot.com/2022/08/the-new-xwayland-extension-is-available.html). _Yes, a new protocol extension for X11._

#### Matrix
* [This Week in Matrix 2022-08-12 | Matrix.org](https://matrix.org/blog/2022/08/12/this-week-in-matrix-2022-08-12). _Nice Nheko updates and more!_

### Worth noting
* [Wanted to confirm some info regarding Pinephone's future... - r/pinephone](https://libreddit.strongthany.cc/r/pinephone/comments/wl1pt3/wanted_to_confirm_some_info_regarding_pinephones/). _In case you've been wondering about 5G and what that means to 4G... the answer is: It really depends on your region and carrier._
* KDE.news: [Akademy 2022 Talk Schedule Now Live](https://dot.kde.org/2022/08/08/akademy-2022-talk-schedule-now-live)
* I ran two polls on how people view the PinePhone: [One on twitter](https://twitter.com/linmobblog/status/1556881856368459784) and [one on the fediverse](https://fosstodon.org/@linmob/108791353451602298).

### Worth reading

#### Competition ;-)
* fossphones: [Linux Phone News - August 9, 2022](https://fossphones.com/08-09-22.html)

#### Tablets
* Plata for LINMOB.net: [Manjaro Plasma on the Microsoft Surface Go 2](https://linmob.net/manjaro-plasma-on-the-surface-go2/). _Great post, thank you for contributing this to linmob.net!_

#### Travels
* Guido Günther: [On a road to Prizren with a Free Software Phone](http://honk.sigxcpu.org/con/On_a_road_to_Prizren_with_a_Free_Software_Phone.html).
* Tobias Bernhard for Purism: [Purism at Berlin Mini GUADEC](https://puri.sm/posts/purism-at-berlin-mini-guadec/).
* Dylan McCall: [GUADEC 2022](https://dylanmc.ca//-/blog/2022/08/10/guadec-2022/).

#### Angelfish on Gaming Handhelds
* kamikazow: [How to use Angelfish as touch-friendly web browser in Steam Deck’s Game Mode](https://kamikazow.wordpress.com/2022/08/14/how-to-use-angelfish-as-touch-friendly-web-browser-in-steam-decks-game-mode/).

#### Waydroid
* Sam Sloniker: [Android apps on Linux with Waydroid [LWN.net]](https://lwn.net/Articles/901459/). _I saw this when it came out, but ... well, better late than never, right?_

#### Sales Fluff
* Purism: [Purism's Librem 5 USA, Privacy-first Smartphones Ship Within 10 Business Days](https://puri.sm/posts/librem-5-usa-privacy-first-smartphones-ship-within-10-business-days/).
* Purism: [What is Special About the Librem 5 USA](https://puri.sm/posts/what-is-special-about-the-librem-5-usa/).

### Worth watching
#### PinePhone with Phosh
* ItsMCB: [What does Purism's Phosh 0.20 environment look like on Manjaro ARM Linux?](https://www.youtube.com/watch?v=sS5TuHAMN_I).

#### L5 USA
* Purism: [What is Special About the Librem 5 USA](https://www.youtube.com/watch?v=M_VujQqYRWg).

#### Failed tablets
* TechHut: [They FAILED us! JingOS and their Linux Tablet](https://www.youtube.com/watch?v=cmBG1Sjgsgk). _Great look back!_

#### PINE64 Community Q&A
* PINE64: [Quarterly Community Q&A [Live Stream] - YouTube](https://www.youtube.com/watch?v=YAviFY3-IIA). _I couldn't be there live and have not watched the recording, so I can't really spill any beans in this comment._

### Something missing?
If your project's cool story (or your awesome video or nifty blog post or ...) is missing and you don't want that to happen again, please get in touch via social media or email!

### Regarding next week
There may be no Weekly Update next week. I am on holidays and may prefer to relax instead. But fear not: I'll definitely have the time to collect to weeks worth of links on August 29th, when we'll be on our way back home by train.

[^1]: I somehow managed to forget this (I blame the temperature in the train) - thanks [João](https://social.librem.one/@joao/108821878497541903) for reminding me!
