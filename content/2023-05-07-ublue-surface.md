+++
title = "Universal Blue (Fedora Kinoite mobile) on the Microsoft Surface Go 2"
date = "2023-05-07T19:00:00Z"
draft = false
[taxonomies]
tags = ["Microsoft Surface Go 2", "Plasma Mobile", "KDE", "tablet"]
categories = ["software"]
authors = ["Plata"]
[extra]
update_note = ""
+++

How OCI images simplify the setup of my Linux tablet.
<!-- more -->

If you read my post [Manjaro Plasma on the Microsoft Surface Go 2](https://linmob.net/manjaro-plasma-on-the-surface-go2/),
you will notice that quite some configuration is required to turn a desktop Linux distribution into something that's fun on mobile.
While none of it is rocket science, it's still work and might not be for the average user who just wants to get their device going with Linux.

So: Is there a way to simplify this?

# OCI defined OS
Of course, one option would be to create a specific distribution/Fedora Spin/Ubuntu flavor..., but that's not too easy either.
Moreover, it just doesn't scale well. After all, there are so many devices out there and a lot of them require a slightly different setup.

Luckily, there's immutable operating systems and [OCI images](https://opencontainers.org/): [Universal Blue](https://ublue.it/) has created a toolbox which allows to
extend [Fedora Silverblue](https://silverblue.fedoraproject.org/) (or [Kinoite](https://kinoite.fedoraproject.org/) etc.) through a
`Containerfile`. This is then built in GitHub actions and uploaded to the [GitHub Container registry](https://docs.github.com/en/packages/working-with-a-github-packages-registry/working-with-the-container-registry) for the world to use.

You can find my take on Fedora 38 Kinoite with [Plasma Mobile](https://plasma-mobile.org/) on [https://github.com/plata/ublue-surface](https://github.com/plata/ublue-surface).

# Features
- Plasma Mobile with Maliit enabled
- Uncluttered homescreen
- [Convergent Windows](https://store.kde.org/p/1985909)
- SDDM scaling
- First run wizard with a selection of (KDE) flatpaks to choose from
- Waydroid

# Installation
> **Disclaimer:** The whole concept is rather new and will require more testing before it can really go to production.
So use at your own risk, as always.

1. Install Fedora Kinoite, see https://kinoite.fedoraproject.org/download/.
2. Rebase via `rpm-ostree rebase ostree-unverified-registry:ghcr.io/plata/ublue-surface:latest`.
3. Reboot.

# Limitations
I didn't manage to include some things (yet). If you have any hints, please get in touch on [GitHub](https://github.com/plata/ublue-surface).

- The Plasma scaling must be changed manually to 200% in System Settings > Display and Monitor > Display Configuration.
- Maliit keyboards for languages other than English must be configured via:
```
gsettings set org.maliit.keyboard.maliit enabled-languages "['en', 'de', 'emoji']"
```
- The [linux-surface](https://github.com/linux-surface/linux-surface) kernel for camera support can be installed via:
```
wget -O /etc/yum.repos.d/linux-surface.repo https://pkg.surfacelinux.com/fedora/linux-surface.repo
wget https://github.com/linux-surface/linux-surface/releases/download/silverblue-20201215-1/kernel-20201215-1.x86_64.rpm
rpm-ostree override replace ./*.rpm
rpm-ostree install surface-secureboot
```
- Firefox doesn't use Wayland. This can be fixed by setting `MOZ_ENABLE_WAYLAND=1` via [Flatseal](https://flathub.org/apps/com.github.tchx84.Flatseal).
- Waydroid doesn't open.
