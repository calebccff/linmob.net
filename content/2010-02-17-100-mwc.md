+++
title = "100! ... MWC!"
aliases = ["2010/02/17/100-mwc/"]
author = "peter"
date = "2010-02-17T15:28:00Z"
layout = "post"
[taxomonomies]
tags = ["bada", "htc", "MeeGo", "moblin", "Motorola", "MWC2k10", "Samsung", "sony ericsson"]
categories = ["events", "hardware", "internal"]
authors = ["peter"]
+++
_While this is the 100th post on this blog which I wanted to kill a few weeks ago in favour of brimborium.net, I will not fuss about why it is so great to have written 100 entries in bad english, but try to summarize what I like about all these new great MWC devices and announcements._

Let's start with the latter. MeeGo. Nokia and Intel join their forces (Maemo and Moblin, respectively), to <del>fight against Android</del> reduce fragmentation and create a strong platform for mobile devices from smartphones to netbooks. Of course nobody knows how this will turn out, but anyway, Nokia and Intel both are pretty strong companys in their markets so this might become a strong platform. I believe it will, there have been some uttering that Nokia and Intel were rather late, but I don't think that this is true, platforms (and markets in general) appear, evolve and become abandonned&mdash;there is nothing like an &#8220;end of history&#8221;, at least if there's enough momentum behind a new player. BTW: The first MeeGo release will be what would have been Moblin 2.2.

That's it with platforms, isn't it? No, it's not. I should mention Samsung's _bada_ (meaning ocean in korean) and of course _Windows Phone 7 Series_, Microsofts new platform for mobile devices. Both are non-PC like operating system (as the old Windows Mobile was), but made for todays social smartphones, which are always connected to social networks&mdash;if you are interested in _Windows Phone 7 Series_, I  recommend to read <a href="http://www.engadget.com/2010/02/17/windows-phone-7-series-everything-you-ever-wanted-to-know/">this engadget article</a>. And _bada_? We know what it is, but we don't really know what the software stack looks like. There might be a Linux kernel, but it might be RTOS, or whatever. The UI is likely to use some librarys used as well in a project which we know as &#8220;Enlightenment&#8221;, as Enlightenment's lead Carsten &#8220;Rasterman&#8221; Haitzler has been working for Samsung lately. I said that we knew what bada is, but did not explain? Well, it is a part of Samsungs strategy to make smartphones more affordable, so one might think of it as an operating system that works on rather poor hardware, something like a &#8220;smartphone operating system for dumbphones&#8221;. But the hardware of Samsungs first _bada_ phone, the Samsung _Wave_, tells a different story. Samsung is creating it's own platform with an application market of it's own, to monetize these smartphones even more&mdash;if you earn you money while the device is being used, you can sell it at lower price points, a simple equation that is, though I believe it is a mix of both strategies mentionned before, the _Wave_ is just high end to get attention for the new platform. We will see how this will evolve, maybe Samsung will abandon some platforms as they have got Android, Windows Mobile and Symbian in their smartphone portfolio, but you never know. Let's just hope that <a href="http://en.wikipedia.org/wiki/Bada_Bing">bada gets a bing widget</a> (or application) ;)

Let's talk about devices. I already mentioned the Wave, we are told that _bada_ runs super fast and that the screen is pretty cool, apparently &#8220;Super AMOLED&#8221; has a fine daylight readability (AMOLED sucked at that), but the Samsung phone I would in fact prefer is the Android beamer phone called, guess it, _Beam_. The inbuilt beamer might be more of a fun feature, but as this beast has a huge battery, it might be interesting, even if you do not plan to use the beamer frequently. BTW, I personally will not get any Samsung phone in the near future, as the SGH-i780 was delightful in terms of build quality, but Samsung was lazy (and is e.g. with the Galaxy) with software updates.

HTC didn't manage to get me surprised, they have announced the &#8220;Desire&#8221; which is, as Android hacker Cyanogen stated, a &#8220;Nexus One done right&#8221;. Ok, it has Sense (which I do not really like), but this thing is unlikely to sell bad. In addition to that there is one more Android phone, the &#8220;Hero&#8221; follow up &#8220;Legend&#8221;, which isn't that legendary.
Of course all these were leaked before, the only thing we were missing were the names.

Motorola... Did they announce anything really cool for the western markets? I guess they did not, and hey, the Droid/Milestone is still pretty cool and there is plenty of time to replace it later this year by another top offering.

Sony Ericsson announced more than one Android phone, having launched more than one new XPeria phone, the X10, X10mini and X10mini pro, which all run a customized UI on top of &#8220;Donut&#8221;. Still the form factor of the mini devices is definitely interesting.

That should be it for smartphones... Oh no, I forgot the upcoming Intel Moorestown based MeeGo beasts, like the Aava Mobile x86 smartphone. Aava Mobile is stating to offer &#8220;The World's First Open Mobile Device&#8221;&mdash;and while we know that this is not true, it is still a pretty cool thing.

Then: Loads of tablets, the first ION2 Netbook, and a device that is my favourite 2010 device until now: The Notion Ink Adam tablet. _But this post is too long as it is, so I will write about that later._
