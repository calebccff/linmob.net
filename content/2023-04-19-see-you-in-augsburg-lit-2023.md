+++
title = "See you at Linux-Infotag in Augsburg, Germany on April 29th, 2023!  "
date = "2023-04-19T18:30:00Z"
draft = false
[taxonomies]
tags = ["Linux-Infotag",]
categories = ["events","shortform",]
authors = ["peter"]
+++

In ten days, on __Saturday, April 29th, 2023, [Augsburger Linux-Infotag 2023](https://www.luga.de/static/LIT-2023/)__ will happen in Augsburg!

And: We're going to have a [Linux on Smartphones stand](https://www.luga.de/static/LIT-2023/stands/#linux-auf-mobilger%C3%A4ten)!
Talk to me, ollie or cahfofpai about the very topic this blog is about, check out devices, and grab a LINMOB sticker! (Natürlich reden wir auch gerne auf Deutsch über Linux-Smartphones!)

So, if you are in the region or can spontaneously make it from farther apart, please come by! You can find the schedule at [Programm - Augsburger Linux-Infotag 2023](https://www.luga.de/static/LIT-2023/program/) - all talks seem to be in German - so keep that in mind!

<!-- more -->

PS: If you want to help with preparations, please contribute to our materials on [cahfofpai / flyer-linux-on-smartphones · framagit](https://framagit.org/cahfofpai/flyer-linux-on-smartphones).


