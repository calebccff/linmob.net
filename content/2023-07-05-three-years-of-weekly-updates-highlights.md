+++
title = "Three Years of Weekly Updates on GNU-like Mobile Linux: Let's summarize and celebrate!"
date = "2023-07-05T20:07:00Z"
draft = false
[taxonomies]
tags = ["weekly update", "celebrations",]
categories = ["commentary", "internal"]
authors = ["Peter"]
[extra]
author_extra = ", Blort, Bobby Hamblin, earboxer, Zach de Cook"
+++

Three years and one day ago, the first Weekly Update on GNU-like mobile Linux, then dubbed LinBits, was published. A lot has happened since, and this celebratory blog post is an attempt to summarize highlights on a per-months basis.

I'd like to thank [Blort](https://social.tchncs.de/@Blort/110569392934238351) for the idea and contributing so much to this post. Also, a huge thanks to Bobby Hamblin, earboxer and Zach de Cook for their contributions to this round-up. Without them, this post would have been far more delayed than just a day.

<!-- more -->

I also want to thank everyone who ever had a kind word for my efforts in summarizing this space, it's unlikely I would have kept this up without you. And, last but not least, I want to thank all the developers who contributed to "Mobile Linux", whether your projects have been mentioned in this post, the weekly updates, or not. Thank you!

Now let's get started!

### July 2020 [Blort]
Phosh 0.4 released with the new feature to scale down apps not fitting a mobile screen.

### August 2020 [Blort]
Megi releases a power management driver for the PinePhone. postmarketOS releases tweaks for Firefox desktop to work on mobile.

### September 2020 [Blort]
Megapixels initially released (on PostmarketOS)

### October 2020 [Blort]
Megapixels gets autofocus. Initial proof of concept release of Biktorgj's open modem firmware for the PinePhone's modem.

### November 2020 [Blort]
Improvements in multitasking via new releases in both Phosh 0.6 and Arch Linux ARM. PINE64 release PinePhone 3gb RAM motherboard upgrades.

### December 2020 [Blort]
KDE Community Edition PinePhone released and Megi starts work on alternate modem firmware for the PinePhone / PinePhone Pro.

### January 2021 [Blort]
Final PinePhone community edition released (Mobian). PostmarketOS switches to eg-25 manager for more reliable calls.  

### February 2021 [Blort]
Sailfish 4 released with PinePhone compatibility. Jumpdrive adds support for the Librem 5.

### March 2021 [Blort]
[First photos from the Librem 5](https://twitter.com/dos1/status/1373447640097112066).

### April 2021 [Blort]
Siglo (mobile Linux PineTime companion) first packaged (Arch Linux ARM) and Phosh 0.10 starts supporting location services.

### May 2021 [Blort]
Power management and performance upgrades to the kernel by Megi and Megapixels 1.0 released with a GPU accelerated viewfinded. 

### June 2021 [Blort]
Crust 0.4 released, lengthening PinePhone's mobile battery life. Plasma Mobile gets a new homescreen.

### July 2021 [Blort]
First video recording on the PinePhone and accelerated video playback on Clapper and µPlayer (newly packaged on Flathub). 

### August 2021 [Blort]
Initial work on WayDroid to run Android apps on mobile (and desktop) Linux using Wayland.

### September 2021 [Blort]
libcamera shown at Embedded Linux Conference with Librem 5 as a target for better mobile linux camera support. Experimental MMS support in Manjaro.

### October 2021 [Bobby Hamblin]
[PinePhone Pro is announced](https://www.pine64.org/2021/10/15/october-update-introducing-the-pinephone-pro/), a mark of maturity in mobile linux platforms.

### November 2021 [Zach DeCook]
[SXMO 1.6.0 is released](https://lists.sr.ht/~mil/sxmo-announce/%3C31IGOQIT71NT5.2LYA6VY1SCLPK%40stacyharper.net%3E), bringing Wayland support with [Sway](https://swaywm.org/) as the default window manager.

### December 2021 [Blort]
[MauiShell](https://nxos.org/maui/introducing-maui-shell/) introduced by NixOS. FluffyChat 1.0 released for mobile Matrix chat.

### January 2022 [Blort] [Earboxer]
PinePhone Pro and accessories [made available to the public](https://www.pine64.org/2022/01/11/pinephone-pro-explorer-edition-pre-orders-open-january-11/). New Uboot release brings suspend to Mobian, PmOS and later other distros. [wvkbd](https://git.sr.ht/~proycon/wvkbd) 0.6 is released with swipe-typing support (some [configuration](https://git.sr.ht/~earboxer/swipeGuess#installationusage-with-wvkbd) needed).

### February 2022 [Blort]
Working suspend on the PinePhone Pro and Librem 5 is able to boot from SD card.

### March 2022 [Blort]
TowBoot now supports PinePhone and PinePhone Pro.

### April 2022 [Blort]
Librem 5 gets the ability to suspend. Improvements to swipe gestures in Plasma.

### May 2022 [Blort]
First photos from the PinePhone Pro released from Megi's test camera app.

### June 2022 [Blort]
Gnome shell on mobile [announced](https://blogs.gnome.org/shell-dev/2022/05/30/towards-gnome-shell-on-mobile/).

### July 2022 [linmob]
VoLTE support in Sailfish OS for a select few devices may not sound like much, but is very important to stay relevant going forward.

### August 2022 [linmob]
Phosh 0.20.0 released, replacing taps with gestures: Home and top bar are now swipable, making the overall experience a lot smoother.
postmarketOS team member Martijn Braam publishes a first blog post on automated phone testing, which is very important work to deliver tested device support without sudden maintainer burnout at some point in time.

### September 2022 [linmob]
tow-boot update support is being added to fwupd, making future tow-boot updates an easier endeavour for users.

### October 2022 [linmob]
libcamera, the open source camera stack and framework for Linux (and Android and ChromeOS), sees its first release, v0.0.1. libcamera has played an important role in getting decent results from Librem 5 and PinePhone Pro cameras since.

### November 2022 [linmob]
Plasma Mobile Gear 22.11, the final Plasma Mobile Gear release, is being published. This may sound bad, but is actually a good thing, as Plasma Mobile apps are a part of Plasma Gear going forward. 

### December 2022 [linmob]
PINE64 annoujnce the PineTab 2, a Rockchip RK3566 powered 10" tablet, which may also be a glimpse at the SoC future for a future PinePhone non-Pro.

### January 2023 [linmob]
Maemo Leste share their progress over a long time span. They move to Devuan Chimaera, and images include apps to make GUI phone calls and messages a reality.

### February 2023 [linmob]
Megi publishes a new release of the PinePhone keyboard firmware, drastically improving power use and thus battery

### March 2023 [linmob]
Ubuntu Touch 20.04 OTA 1 released, rebasing Ubuntu Touch from 16.04 to a 4 years newer base level. This included many challenges, including switching from upstart to systemd.

### April 2023 [linmob]
PINE64 surprises with offering a RISC-V PineTab, the PineTab V, for pre-order.

### May 2023 [Zach DeCook]
Phosh's first [swipe-typing demo](https://social.librem.one/@agx/110260534404795348) shown with phosh-osk-stub.

### June 2023 [linmob]
Megi presents a u-boot boot menu for the PinePhone Pro, making a future multi boot image possible.


### That's it

This is it, thank you for reading! What did we miss? And what do you hope for the next three years?


