+++
title = "Weekly GNU-like Mobile Linux Update (28/2023): How to avoid notches in Phosh and Snaps on Ubuntu Touch"
date = "2023-07-16T16:45:00Z"
updated = "2023-07-16T17:54:50Z"
draft = false
[taxonomies]
tags = ["Sailfish OS", "Ubuntu Touch", "Nemo Mobile"]
categories = ["weekly update"]
authors = ["Peter",]
[extra]
author_extra = " (with friendly assistance from plata's awesome script)"
update_note = "Added LicheePhone bit."
+++

Also: libcamera 0.1.0, Photography on the PinePhone Pro running Sailfish OS, a status update on the Juno tablet, the PineTab V approaching assembly and more! 

<!-- more -->
_Commentary in italics._

### Hardware

Apparently, [there won't be a TH1520-powered LicheePhone](https://wiki.postmarketos.org/wiki/Category:Sipeed#Lichee_Pi_4A_Portable_Devices_2023),  due to excessive power consumption. The phone form-factor is going to be re-evaluated with future SoCs. _Honestly: This is a good decision. Also, thank you, Schimon, for sending this in!_

### Software

#### Gnome Ecosystem
- This Week in GNOME: [#104 Full Text Search](https://thisweek.gnome.org/posts/2023/07/twig-104/)
- chergert: [Spellchecking for GTK 4](https://blogs.gnome.org/chergert/2023/07/12/spellchecking-for-gtk-4/)
- Matthew Garrett: [Roots of Trust are difficult](https://mjg59.dreamwidth.org/66907.html)
- Base-Art: [GNOME Web Canary is back](https://base-art.net/Articles/gnome-web-canary-is-back/)

#### Phosh
- Phosh.mobi blog: [Avoiding notches](https://phosh.mobi/posts/notch-support/)
- Guido Günther: [If your device has a notch or cutout and you want #phosh to handle it automatically check https://phosh.mobi/posts/notch-support/ on how to describe the display properties.](https://social.librem.one/@agx/110696665305178881)
- Guido Günther: [If you want to prevent phosh from taking notches into account when laying out elements in the top bar use gsettings set sm.puri.phosh shell-layout &apos;none&apos;](https://social.librem.one/@agx/110679089893652641)

#### Plasma Ecosystem
- Nate Graham: [No TWiK post this week](https://pointieststick.com/2023/07/15/no-twik-post-this-week/)
- Nate Graham: [This week in KDE: Akademy approaches](https://pointieststick.com/2023/07/07/this-week-in-kde-akademy-approaches/). _Just in case you read the last one in the first hour and missed this._
- > 🐰: [I want a clean config directory!](https://rabbitictranslator.com/kconfig/)
- KDE Announcements: [KDE Ships Frameworks 5.108.0](https://kde.org/announcements/frameworks/5/5.108.0/)

#### Sailfish OS
- adampigg: [One of the first JPEG images taken with the PinePhonePro, and my @libcamera based app Shutter.Colours are a little dark, and there is a weird pattern of dots, but ill take that as a win anyway!](https://fosstodon.org/@piggz/110702693585499602)

#### Ubuntu Touch
- Unofficial Ubuntu Touch for PinePhone (Pro): [v0.7.1](https://gitlab.com/ook37/pinephone-pro-debos/-/tags/v0.7.1)
- fredldotme: [Just released Snapz0r on the Open Store, enabling Snap support on Ubuntu Touch with ease!](https://mastodon.social/@fredldotme/110723317597670107). _Even if you (like me) are not a Snap superfan, this is great news!_
- fredldotme: [The levels of completeness at play here just massage my brain right!Tide, originally an iPad app, built as a Snap on the Ubuntu Touch tablet itself, using Snapcraft.](https://mastodon.social/@fredldotme/110719591613134395)

#### Nemo Mobile
- Jozef Mlich on Fosstodon: [Have you missed my talk about #NemoMobile at #DevConf? Heads up! Recordings are online.](https://fosstodon.org/@jmlich/110702165142051688)

#### Distributions
- Manjaro PinePhone Phosh: [Beta 34](https://github.com/manjaro-pinephone/phosh/releases/tag/beta34)

#### Linux
- Phoronix: [Linux 6.6 Will Make It Easy To Disable IO_uring System-Wide](https://www.phoronix.com/news/Linux-6.6-sysctl-IO_uring)
- Phoronix: [Open-Source Graphics Driver Updates Begin Queuing For Linux 6.6](https://www.phoronix.com/news/Linux-6.6-Initial-DRM-Misc-Next)
- Phoronix: [Linux 6.5 Features From USB4 v2 To More WiFi 7, Unaccepted Memory, Scope-Based Resource Management](https://www.phoronix.com/review/linux-65-features)
- Phoronix: [Imagination Tech Rolls Out Latest PowerVR Rogue DRM Kernel Driver Patches](https://www.phoronix.com/news/Imagination-PowerVR-DRM-v4)

#### Stack
- Phoronix: [Mesa 23.2 Feature Development Concludes With Numerous New Vulkan Extensions](https://www.phoronix.com/news/Mesa-23.2-Branched)
- [libcamera/libcamera.git - libcamera 0.1.0 release](https://git.libcamera.org/libcamera/libcamera.git/tag/?h=v0.1.0)

#### Matrix
- Matrix.org: [This Week in Matrix 2023-07-14](https://matrix.org/blog/2023/07/twim/)

### Worth Noting
- PINE64official (Reddit): [Which one is best for daily driver, Pinephone Pro or Pinephone beta edition?](https://www.reddit.com/r/PINE64official/comments/150ee2f/which_one_is_best_for_daily_driver_pinephone_pro/)
- Lemmy - linuxphones: [can an average person use a linux phone?](https://lemmy.ml/post/2056302)
- PinePhone (Reddit): [What is the bottleneck when it comes to linux phones becoming mainstream?](https://www.reddit.com/r/pinephone/comments/14vvhep/what_is_the_bottleneck_when_it_comes_to_linux/) _Spoiler: It's more than one bottleneck._
- Purism community: [Updated L5 -> Files, PuroStore - Gone!](https://forums.puri.sm/t/updated-l5-files-purostore-gone/20826) _Oops._
- Purism community: [XMPP: Dino for Librem 5](https://forums.puri.sm/t/xmpp-dino-for-librem-5/20806)
- Purism community: [Screen calls (voice to text)](https://forums.puri.sm/t/screen-calls-voice-to-text/20786). _Good idea!_
- [Martijn Braam: "First #librem5 frame in #megapixels"](https://fosstodon.org/@martijnbraam/110701204177516270)
- [Yoshi: "By now I've been home for a wh…"](https://chaos.social/@yoshi/110719940198744768). _PinePhone Pro impressions!_
- [𖤐 MadameMalady 𖤐: "I released a very alpha version of My Waydroid Manager as a python / bash program with #gtk and #libadwaita ui today!"](https://strangeobject.space/@FOSSingularity/110719909003508492)
- [Lup Yuen Lee 李立源: "#RISCV JH7110 "PineTab-V mainboard fresh from production…"](https://qoto.org/@lupyuen/110716173552655516)
- [Robert Mader: "#ArchLinux / #danctnix now ships #Linux #megi 6.4 which has all #PinePhonePro patches for correct camera rotation and location (front/back - used for automatic mirroring in some apps and better camera naming).…"](https://floss.social/@rmader/110706992377374314)
- [caleb: "LVG/libinput multitouch hack, …"](https://social.treehouse.systems/@cas/110696937352343680)

### Worth Reading
- Purism: [Purism and Linux 6.2 to 6.4](https://puri.sm/posts/purism-and-linux-6-2-to-6-4/)
- Christian Peter: [Forensische Sicherungsmöglichkeiten von Linux-Betriebssystemen auf Mobilgeräten](https://it-forensik.fiw.hs-wismar.de/index.php/Christian_Peter) _This is a bachelor thesis in German. Thank you, Christian, for telling me about this, nice work!_

### Worth Watching
- Adrian Campos Garrido: [Mobile devices and openSUSE, is it posible?](https://media.ccc.de/v/4199-mobile-devices-and-opensuse-is-it-posible)
- The Linux Foundation: [Camera Applications with Libcamera and PipeWire - Daniel Scally, Ideas on Board Oy](https://www.youtube.com/watch?v=lxZA_Egnc0o)
- Linux Stuff: [Juno Tablet - Status July 2023](https://www.youtube.com/watch?v=fbNiLiRkzIE)
- CodeInc: [PostmarketOS 23.06 Arrives for Linux Phones](https://www.youtube.com/watch?v=4tNfqc-esAQ)
- RikerLinux: [Manjaro Mobile - Linux Mobile for your Smartphone](https://www.youtube.com/watch?v=MKUECNtcSwc)
- Anino Ni Kugi: [Trying out Ubuntu Touch on an External Display | Fairphone 4](https://www.youtube.com/watch?v=q7BMSI6LgMI)
- Santosh Das: [Ubuntu touch installation in one plus 5](https://www.youtube.com/watch?v=30TUU0vNXkY)
- YouTube: [Fairphone 4 with Ubuntu Touch on External Display](https://www.youtube.com/watch?v=e1NSOznvLIc)
- Continuum Gaming: [Continuum Gaming E373: Sailfish OS Update 4.5.0.21](https://www.youtube.com/watch?v=Ozi8wZT9inU)
- Hugh Jeffreys: [Worst Phone I Ever Bought - Jolla Phone - SailfishOS](https://www.youtube.com/watch?v=HzCMKbhK-EY). _Why?! Seriously, complaining about hours of time spent after buying an item known to be "permanently locked" is beyond me. Of course there are going to be dragons!_
- Joel Martin: [GnuRadio fonctionnel sur le smartphone linux PinePhone Pro avec le HackRF One 👍](https://www.youtube.com/watch?v=xNgLkhVcqsk)

### Thanks
Huge thanks to Plata for [the nifty set of Python scripts](https://framagit.org/linmob/linmob.frama.io/-/merge_requests/5) that speed up collecting links from feeds by a lot.

### Something missing? Want to contribute?
If your project's cool story (or your awesome video or nifty blog post or ...) is missing and you don't want that to happen again, please just put it into [the hedgedoc pad](https://pad.hacc.space/7yCLy5a9QyOLWusIFiTt9A?edit) for the next one! Since I am collecting many things there, this get's you early access if you will ;-) __If you just stumble on a thing, please put it in there too - all help is appreciated!__

