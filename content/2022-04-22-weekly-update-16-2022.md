+++
title = "Weekly #LinuxPhone Update (16/2022): KDE Gear 22.04, postmarketOS v21.12 SP4 and ... never mind"
date = "2022-04-22T21:50:00Z"
updated = "2022-04-23T09:35:00Z"
[taxonomies]
tags = ["PinePhone","PinePhone Pro","Tow-Boot","Volla","postmarketOS",]
categories = ["weekly update"]
authors = ["peter"]
[extra]
update_note="Don't get overly excited about Organic Maps, maybe try Osmin instead."
+++

_In other news:_ Shitphones are smart, a new phone that's going to be available with Ubuntu Touch, fun handheld hardware, funding for GNOME Shell Mobile and a Squeekboard bugfix release.
<!-- more -->

_Commentary in italics._

### Hardware
* Liliputing: [Volla Phone 22 runs Ubuntu Touch or a privacy-focused Android fork… or both (crowdfunding)](https://liliputing.com/2022/04/volla-phone-22-runs-ubuntu-touch-or-a-privacy-focused-android-fork-or-both-crowdfunding.html). _While its an unlikely target for mainline Linux, its nice to see another Phone that officially supports Ubuntu Touch. Unless I am completely mistaken, its based on the Gigaset GS5 phone, which is definitely [decent](https://www.notebookcheck.net/Gigaset-GS5-review-Gentle-upgrade-for-the-smartphone-made-in-Germany.584868.0.html)._


### Software progress

#### GNOME ecosystem
* This Week in GNOME: [#40: Rows and Containers](https://thisweek.gnome.org/posts/2022/04/twig-40/). _Nice improvements - and it looks like someone needs to add another app to LinuxPhoneApps.org!_
* Ergaster: [Funding decentralised/local-first applications for GNOME](https://blog.ergaster.org/post/20220422-decentralised-local-first-applications/).
* chergert: [Porting GNOME Builder to GTK 4](https://blogs.gnome.org/chergert/2022/04/22/porting-gnome-builder-to-gtk-4/).
* [Squeekboard had a bugfix release (1.17.1)](https://forums.puri.sm/t/squeekboard-bugfix-1-17-1/17003/2), fixing a rather bad scaling bug.

#### Plasma/Maui ecosystem
* Felipe Kinoshita: [Tasks](https://fhek.gitlab.io/en/tasks/). _Nice little app!_
* KDE Announcements: [KDE Gear 22.04](https://kde.org/announcements/gear/22.04.0/). _Nice improvements to Elisa, KDE Itinerary and Kalendar is now part of Gear!_

#### Sailfish OS
*  flypig: [Sailfish Community News, 21st April, Sailfish SDK 3.9](https://forum.sailfishos.org/t/sailfish-community-news-21st-april-sailfish-sdk-3-9/11221). _Must read for Sailfish fans :)_ 

#### Maemo Leste
* Maemo Leste: [Sixteenth Update: November and December 2021, January - April 2022](https://maemo-leste.github.io/maemo-leste-sixteenth-update-november-and-december-2021-january-april-2022.html). _Great progress! While the Conversations and phone app still require turning on the beowulf-devel repo, this is really impressive._

#### Distro news

* postmarketOS: [v21.12 Service Pack 4](https://postmarketos.org/blog/2022/04/18/v21.12.4-release/). _Great, incremental improvements for postmarketOS's stable release!_

#### Kernel stuff
* Phoronix: [MediaTek Preparing Stateless AV1 Video Linux Decode Driver For Newer SoCs](https://www.phoronix.com/scan.php?page=news_item&px=MediaTek-AV1-Decode-Linux).

#### WPE WebKit
* Chris Lord: [WebKit frame delivery](https://www.chrislord.net/2022/04/19/webkit-frame-delivery/). _Nice performance improvements on Wayland!_

#### Running Software for other architectures
* Phoronix: [Box86 0.2.6 / Box64 0.1.8 Released With Working Steam & Steam Play On Non-x86 CPUs](https://www.phoronix.com/scan.php?page=news_item&px=Box86-0.2.6-Box64-0.1.8).
* Phoronix: [QEMU 7.0 Released With Intel AMX Support, Many RISC-V Additions](https://www.phoronix.com/scan.php?page=news_item&px=QEMU-7.0-Released).

### Worth noting

* If you've ever wondered how to use the real [GNOME Desktop on a Linux Phone, PineFLOWn has some nice info for you](https://forum.pine64.org/showthread.php?tid=16445&highlight=pinephone+gnome+42)!
* [Organic Maps is now on Flathub](https://beta.flathub.org/apps/details/app.organicmaps.desktop). _Obviously, due to writing this update, I did not manage to try the app yet, but it's definitely exciting! **Update 2022-04-23**: Never mind. It's not for Linux Phones (yet)._
* [GNOME Shell Mobile gets funding from Germany](https://forums.puri.sm/t/gnome-shell-mobile-gets-funding-from-germany/17002). _Finally something useful done with my taxes! ;-)_

### Worth reading

#### Smartphones are shit
* Jack Leightcap: [Smartphones are Shit: the Case for LineageOS and the Pinephone](https://jleightcap.srht.site/blog/openphone.html). _Check the hn [discussion](https://news.ycombinator.com/item?id=31076036) if you feel like it.
* Matthew Garrett: [The Freedom Phone is not great at privacy](https://mjg59.dreamwidth.org/59479.html). _Wasn't there a tradeoff between freedom and security, anyway? On a more serious note: This is even worse than I expected._

#### Impressions of Ubuntu Touch
* mr.smashy: [Linux for Smartphones](https://medium.com/codex/linux-for-smartphones-2eca6340f43f).  

#### Sxmo on powerful hardware
* Anjandev Momi: [Sxmo on the poco f1](https://momi.ca/posts/2022-04-19-sxmopocof1.html). _Nice notch-handling!_

#### Impressions of postmarketOS
* Jesse Smith for Distrowatch: [postmarketOS on the PinePhone](https://distrowatch.com/weekly-mobile.php?issue=20220418#postmarketos).

#### Tutorials
* Neil Brown: [A global "scale-to-fit" for the PinePhone Pro](https://neilzone.co.uk/2022/04/a-global-scale-to-fit-for-the-pinephone-pro). _It's quick and simple, but I still stand by [my recommendation](https://linmob.net/pinephone-setup-scaling-in-phosh/#scaling-generally-normal-apps)._

#### More Hardware from Germany (well, maybe)
* TuxPhones: [The PocketReform is a made-in-Berlin Linux handheld](https://tuxphones.com/mnt-pocketreform-open-hardware-linux-pda-keyboard-arm/). _Nice, but I am not sold on that keyboard._

#### Fluff
* Purism: [The Future of Social Media Depends on You](https://puri.sm/posts/the-future-of-socialmedia/).

### Worth watching

#### PinePhone (Pro)
* Ivon Huang: [PinePhone runs without a battery](https://www.youtube.com/watch?v=CA6L_Q5dr88).
* ItsMCB: [Installing Tow-Boot On My Pine64 PinePhone Pro](https://www.youtube.com/watch?v=7piXOM8G2uQ).

#### Ubuntu Touch
* CyberPunked: [Ubuntu Touch - Google Pixel 3a (Sargo) - OTA-22 (2022-04-15)](https://www.youtube.com/watch?v=gpgUNv2XCEQ).
* CyberPunked: [Ubuntu Touch - OnePlus 6t / fajita (devel 2022-04-18) - double tap to wake](https://www.youtube.com/watch?v=vCZ8W9wclek).
* anino207: [Hidden Feature in Ubuntu Touch Keyboard #shorts](https://www.youtube.com/shorts/VInFvqNDOAU).
* anino207: [Ubuntu Touch Gestures](https://www.youtube.com/watch?v=E54Yp_XYgPg).

#### Cyber Decks
* ETA Prime: [Raspberry Pad 5, You Can Easily Build An Awesome Cyber Deck With This! Hands-On](https://www.youtube.com/watch?v=GjEcL7u8wjA). _Nice!_

#### Abusing (?) postmarketOS
* Le.Chiffre: [DIY: 10$ Raspberry Pi 4 alternative Samsung A5 with Alpine Linux PostmarketOS SSH docker pihole](https://www.youtube.com/watch?v=0HemPRAq7dc).


### Something missing?
If your projects' cool story (or your awesome video or nifty blog post or ...) is missing and you don't want that to happen again, please get in touch via social media or email!




